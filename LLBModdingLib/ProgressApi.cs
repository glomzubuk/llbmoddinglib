﻿using System;
using LLHandlers;

namespace LLBML
{
    /// <summary>
    /// A wrapper to the game's obfuscated Progress class.
    /// </summary>
    public static class ProgressApi
    {
#pragma warning disable 1591
        public static int GetCurrency() => BDCINPKBMBL.currency;
        public static int GetXp() => BDCINPKBMBL.xp;

        #region arcade
        public static bool DidCharacterCompleteArcade(Character pChar) =>
            EPCDKLCABNC.HGHKKPCAKOM(pChar);

        public static bool DidAllCharactersCompleteArcade() =>
            EPCDKLCABNC.JIAKEINJLMN();

        public static void CharacterCompleteArcadeSet(Character pChar) =>
            EPCDKLCABNC.NHANFBFHNDJ(pChar);
        #endregion

        #region unlocks
        public static bool IsAvaliableForUnlocking(AudioTrack track) =>
            EPCDKLCABNC.ENLLEDKHNAH(track);

        public static bool IsAvaliableForUnlocking(Character pChar, CharacterVariant pCharVar) =>
            EPCDKLCABNC.ENLLEDKHNAH(pChar, pCharVar);

        public static bool AllOutfitsAboveModelAlt2AreUnlocked(Character pChar) =>
            EPCDKLCABNC.NHMBIMHGDNH(pChar);

        public static bool IsUnlocked(AudioTrack track) => EPCDKLCABNC.KFFJOEAJLEH(track);

        public static bool IsUnlocked(Character pChar, CharacterVariant pCharVar, int peerPlayerNr = -1) => 
            EPCDKLCABNC.KFFJOEAJLEH(pChar, pCharVar, peerPlayerNr);

        public static bool IsUnlocked(Character pChar, int peerPlayerNr = -1) => 
            EPCDKLCABNC.KFFJOEAJLEH(pChar, peerPlayerNr);

        public static bool IsUnlocked(GameMode pGameMode) => EPCDKLCABNC.KFFJOEAJLEH(pGameMode);

        public static bool IsUnlocked(Stage pStage) => EPCDKLCABNC.KFFJOEAJLEH(pStage);

        public static bool IsUnlocked(UnlockableMode pMode) => EPCDKLCABNC.KFFJOEAJLEH(pMode);

        public static bool IsDefaultUnlocked(Character pChar)
        {
            switch (pChar)
            {
                case Character.BOSS:
                case Character.COP:
                case Character.ELECTRO:
                case Character.GRAF:
                    return false;
            }
            return true;
        }

        public static bool IsDefaultUnlocked(Stage pStage)
        {
            switch (pStage)
            {
                case Stage.OUTSKIRTS:
                case Stage.SEWERS:
                case Stage.CONSTRUCTION:
                case Stage.SUBWAY:
                case Stage.POOL:
                case Stage.ROOM21:
                    return true;
            }
            return false;
        }

        public static string GetSkinName(Character character, CharacterVariant characterVariant) =>
            EPCDKLCABNC.CPNGJKMEOOM(character, (int)characterVariant);

        public static string GetStageName(Stage stage) =>
            EPCDKLCABNC.IHODJKJJMLE(stage);

        #endregion
#pragma warning restore 1591
    }
}
