﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;
using BepInEx;
using BepInEx.Configuration;
using BepInEx.Logging;
using LLBML.UI;
using LLBML.States;

namespace LLBML.Utils
{

    /// <summary>
    /// Handles the paths for centralising user-supplied information.
    /// </summary>
    public static class ModdingFolder
    {
        private static readonly ManualLogSource Logger = LLBMLPlugin.Log;

        private static string DefaultModdingFolderPath => Path.Combine(BepInEx.Paths.GameRootPath, "ModdingFolder");
        //private static string DefaultModdingFolderPath => Path.Combine(Path.GetDirectoryName(LLBMLPlugin.Instance.Info.Location), "ModdingFolder");

        private static ConfigEntry<string> moddingFolderPath;
        private static ConfigEntry<bool> doneTutorial;

        internal static void InitConfig(ConfigFile config)
        {
            moddingFolderPath = config.Bind<string>(
                "ModdingFolder",
                "ModdingFolderPath",
                DefaultModdingFolderPath,
                new ConfigDescription(
                    "The path of the folder that will be used to store mod's user provided information.",
                    null,
                    "modmenu_directorypicker"
                )
            );
            doneTutorial = config.Bind<bool>(
                "ModdingFolder",
                "DoneTutorial",
                false,
                new ConfigDescription(
                    "Whether or not the user was guided on using the modding folder.",
                    null,
                    "modmenu_hidden"
                )
            );
            /* TODO Remake the Modding folder tutorial when Modmenu supports oppening a specific mod setting
            if (!doneTutorial.Value && moddingFolderPath.Value == DefaultModdingFolderPath)
            {
                GameEvents.MenuEvents.OnMainMenuFirstEntered += (o, a) =>
                {
                    if (ModDependenciesUtils.IsModLoaded(ModDependenciesUtils.ModMenuGUID))
                    LLBMLPlugin.Instance.StartCoroutine(StartTutorial());
                };
            }
            else
            {
                Directory.CreateDirectory(moddingFolderPath.Value);
            }
            */
            if (moddingFolderPath.Value.Contains("steamapps"))
            {
                moddingFolderPath.Value = DefaultModdingFolderPath;
            }
            Directory.CreateDirectory(moddingFolderPath.Value);
        }
        private static IEnumerator StartTutorial()
        {
            yield return new WaitForSeconds(0.1f);
            int doTutorial = -1;
            Dialogs.OpenConfirmationDialog(
                "Modding Folder",
                "Looks like you didn't set your modding folder yet!\nWould you like to set it now? =)",
                () => {
                    doTutorial = 1;
                },
                () => {
                    doTutorial = 0;
                }
            );
            yield return new WaitWhile(() => doTutorial == -1);
            if (doTutorial == 0)
            {
                doneTutorial.Value = true;
                yield break;
            }
            GameStates.Send(Msg.SEL_OPTIONS, -1, -1);

            yield return new WaitForSeconds(0.1f);

            bool oked = false;
            Dialogs.OpenDialog(
                "Modding Folder",
                "Go into 'mod settings', then LLBModdingLib.\nYou can then use the browse button to select a new folder.",
                "Ok",
                () => {
                    oked = true;
                }
            );
            yield return new WaitWhile(() => oked);

            doneTutorial.Value = true;

            yield break;
        }

        /// <summary>
        /// Gets the mod's own subfolder located within the user-defined modding folder.
        /// </summary>
        /// <param name="modInfo">The mod to get the subfolder for.</param>
        /// <returns>The mod's subfolder.</returns>
        public static DirectoryInfo GetModSubFolder(PluginInfo modInfo)
        {
            return GetModdingFolder().CreateSubdirectory(modInfo.Metadata.Name);
        }

        /// <summary>
        /// Gets the user-defined modding folder.
        /// </summary>
        /// <returns>The folder that contains all mods user-supplied data.</returns>
        public static DirectoryInfo GetModdingFolder()
        {
            DirectoryInfo moddingFolder = new DirectoryInfo(moddingFolderPath.Value);
            if (!moddingFolder.Exists) {
                if (moddingFolderPath.Value == DefaultModdingFolderPath)
                {
                    moddingFolder.Create();
                }
                else
                {
                    Logger.LogError("[ModdingFolder] Invalid path: " + moddingFolder.FullName);
                    return null;
                }
            }
            return moddingFolder;
        }
    }
}
