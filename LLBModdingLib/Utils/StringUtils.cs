﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using LLHandlers;
namespace LLBML.Utils
{
    /// <summary>
    /// A class to hold <see cref="string"/> related functions.
    /// </summary>
    public static class StringUtils
    {
        /// <inheritdoc cref="CharacterApi.GetCharacterByName(string)"/>
        [Obsolete("Utils.StringUtils.GetCharacterByName() is deprecated, please use CharacterApi.GetCharacterByName() instead.")]
        public static Character GetCharacterByName(string characterName)
            => CharacterApi.GetCharacterByName(characterName);

        /// <inheritdoc cref="CharacterApi.TheWitcherGetCharacterByName(string)"/>
        [Obsolete("Utils.StringUtils.TheWitcherGetCharacterByName() is deprecated, please use CharacterApi.TheWitcherGetCharacterByName() instead.")]
        public static Character TheWitcherGetCharacterByName(string characterName)
            => CharacterApi.TheWitcherGetCharacterByName(characterName);

        /// <summary>
        /// Mapping from <see cref="Character"/> to <see cref="string"/> for character names.
        /// </summary>
        public static Dictionary<Character, string> characterNames = new Dictionary<Character, string>
        {
            [Character.KID] = "Raptor",
            [Character.ROBOT] = "Switch",
            [Character.CANDY] = "Candyman",
            [Character.ELECTRO] = "Grid",
            [Character.BOOM] = "Sonata",
            [Character.CROC] = "Latch",
            [Character.PONG] = "Dice",
            [Character.BOSS] = "Doombox",
            [Character.COP] = "Nitro",
            [Character.SKATE] = "Jet",
            [Character.GRAF] = "Toxic",
            [Character.BAG] = "Dust&Ashes",
        };


        /// <summary>
        /// Returns a machine safe name for the specified character.
        /// </summary>
        /// <returns>The machine safe name.</returns>
        /// <param name="character">The character.</param>
        public static string GetCharacterSafeName(Character character)
        {
            //TODO do it better than that, i mean common
            string charName = characterNames[character];
            if (character == Character.BAG)
            {
                charName.Replace('&', 'N');
            }
            return charName;
        }

        /// <summary>
        /// Mapping from <see cref="Stage"/> to <see cref="string"/> for regular stages names.
        /// </summary>
        public static Dictionary<Stage, string> regularStagesNames = new Dictionary<Stage, string>
        {
            [Stage.OUTSKIRTS] = "Outskirts",
            [Stage.SEWERS] = "Sewers",
            [Stage.JUNKTOWN] = "Desert",
            [Stage.CONSTRUCTION] = "Elevator",
            [Stage.FACTORY] = "Factory",
            [Stage.SUBWAY] = "Subway",
            [Stage.STADIUM] = "Stadium",
            [Stage.STREETS] = "Streets",
            [Stage.POOL] = "Pool",
            [Stage.ROOM21] = "Room21",
        };

        /// <summary>
        /// Mapping from <see cref="Stage"/> to <see cref="string"/> for retro stages names.
        /// </summary>
        public static Dictionary<Stage, string> retroStagesNames = new Dictionary<Stage, string>
        {
            [Stage.OUTSKIRTS_2D] = "Retro Outskirts",
            [Stage.POOL_2D] = "Retro Pool",
            [Stage.SEWERS_2D] = "Retro Sewers",
            [Stage.ROOM21_2D] = "Retro Room21",
            [Stage.STREETS_2D] = "Retro Streets",
            [Stage.SUBWAY_2D] = "Retro Train",
            [Stage.FACTORY_2D] = "Retro Factory",
        };

        /// <summary>
        /// Returns a readable name for the specified stage.
        /// </summary>
        /// <returns>The stage's human readable name.</returns>
        /// <param name="stage">The stage.</param>
        public static string GetStageReadableName(Stage stage)
        {
            if(StageHandler.Is2D(stage))
            {
                return retroStagesNames[stage];
            }
            else
            {
                return regularStagesNames[stage];
            }
        }

        /// <summary>
        /// Converts a byte to a binary string representation.
        /// </summary>
        /// <param name="input">The byte to convert.</param>
        public static string ByteToString(byte input)
        {
            return Convert.ToString(input, 2).PadLeft(8, '0');
        }

        /// <summary>
        /// Converts a byte array to a binary string representation array.
        /// </summary>
        /// <param name="input">The byte array to convert.</param>
        public static string[] BytesToStrings(byte[] input)
        {
            return input.Select(ByteToString).ToArray();
        }

        /// <summary>
        /// Converts a byte array to a hex string representation array.
        /// </summary>
        /// <param name="input">The byte array to convert.</param>
        public static string BytesToHexString(byte[] input)
        {
            return BitConverter.ToString(input).Replace("-", "");
        }

        /// <summary>
        /// Converts a bytes to a binary string representation.
        /// </summary>
        /// <param name="input">The byte array to convert.</param>
        public static string PrettyPrintBytes(byte[] input)
        {
            return string.Join(" ", BytesToStrings(input));
        }
    }
}
