﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Security.Cryptography;
using BepInEx;
using BepInEx.Bootstrap;
using BepInEx.Logging;

namespace LLBML.Utils
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class BepInRef : Attribute
    {
        private static readonly ManualLogSource Logger = LLBMLPlugin.Log;

        public static void RefCheck()
        {
            foreach (var entry in Chainloader.PluginInfos)
            {
                PluginInfo info = entry.Value;
                var customAttributes = (BepInRef[])info.Instance.GetType().GetCustomAttributes(typeof(BepInRef), true);
                if (customAttributes.Length > 0)
                {
                    string checksumPath = Path.Combine(Path.GetDirectoryName(info.Location), "cksm");
                    if (File.Exists(checksumPath))
                    {
                        char[] charsum = File.ReadAllText(checksumPath).TrimEnd((char)10).ToCharArray();
                        for (int i = 0; i < charsum.Length; i++)
                        {
                            charsum[i] = hexmap[charsum[i]];
                        }
                        string checksum = new string(charsum);
                        using (var stream = File.OpenRead(info.Location))
                        {
                            using (var md5 = MD5.Create())
                            {
                                var hash = md5.ComputeHash(stream);
                                var stringHash = BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();

                                if (checksum == stringHash)
                                {
                                    return;
                                }
                            }
                        }
                    }
                    UnityEngine.Object.Destroy(info.Instance);
                }
            }
        }
        private static readonly Dictionary<char, char> hexmap = new Dictionary<char, char>
        {
            ['c'] = '0',
            ['d'] = '1',
            ['e'] = '2',
            ['f'] = '3',
            ['0'] = '4',
            ['1'] = '5',
            ['2'] = '6',
            ['3'] = '7',
            ['4'] = '8',
            ['5'] = '9',
            ['6'] = 'a',
            ['7'] = 'b',
            ['8'] = 'c',
            ['9'] = 'd',
            ['a'] = 'e',
            ['b'] = 'f',
        };
    }
}
