﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace LLBML.Utils
{
    /// <summary>
    /// Utils for generics or types operations.
    /// </summary>
    public static class GenericUtils
    {
        /// <summary>
        /// Gets enum values from the type via reflection.
        /// </summary>
        /// <typeparam name="T">The targeted enum type.</typeparam>
        /// <returns>The enum values.</returns>
        public static IEnumerable<T> GetEnumValues<T>() where T : new()
        {
            T valueType = new T();
            return typeof(T).GetFields()
                .Select(fieldInfo => (T)fieldInfo.GetValue(valueType))
                .Distinct();
        }
    }

    /// <summary>
    /// Useful extensions to the Array type.
    /// </summary>
    public static class ArrayExtension
    {
        public static T[] Add<T>(this T[] target, params T[] items)
        {
            // Validate the parameters
            if (target == null)
            {
                target = new T[] { };
            }
            if (items == null)
            {
                items = new T[] { };
            }

            // Join the arrays
            T[] result = new T[target.Length + items.Length];
            target.CopyTo(result, 0);
            items.CopyTo(result, target.Length);
            return result;
        }
        public static void Fill<T>(this T[] originalArray, T with) {
            for(int i = 0; i < originalArray.Length; i++){
                originalArray[i] = with;
            }
        }
    }

    /// <summary>
    /// Useful extensions to the Texture2D type.
    /// </summary>
    public static class Texture2DExtension
    {
        public static Color GetRelativePixel(this Texture2D texture, int x, int y, int originalWidth = 512, int originalHeight = 512)
        {
            return texture.GetRelativePixel(new Vector2(x, y), new Vector2(originalWidth, originalHeight));
        }
        public static Color GetRelativePixel(this Texture2D texture, Vector2 coords, Vector2 orginalSize)
        {
            var size = new Vector2(texture.width, texture.height);
            Vector2 relativeCoords = (size * coords) / orginalSize;
            return texture.GetPixel((int)relativeCoords.x, (int)relativeCoords.y);
        }
    }
}
