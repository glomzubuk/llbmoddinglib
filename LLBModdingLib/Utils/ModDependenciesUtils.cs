﻿using System;
using System.Reflection;
using System.Collections.Generic;
using BepInEx;
using BepInEx.Logging;
using HarmonyLib;

namespace LLBML.Utils
{
    /// <summary>
    /// A class that helps abstracting soft dependencies make maintaining compatibility with LLBMM easier
    /// </summary>
    public static class ModDependenciesUtils
    {
        private static readonly ManualLogSource Logger = LLBMLPlugin.Log;

        /// <summary>
        /// Returns whether or not the mod was present in the BepInEx Chainloader.
        /// </summary>
        /// <returns><c>true</c>, if mod was present, <c>false</c> otherwise.</returns>
        /// <param name="modGUID">Mod GUID.</param>
        public static bool IsModPresent(string modGUID)
        {
            return BepInEx.Bootstrap.Chainloader.PluginInfos.ContainsKey(modGUID);
        }

        /// <summary>
        /// Returns whether or not the mod was present in the BepInEx Chainloader.
        /// </summary>
        /// <returns><c>true</c>, if mod was present, <c>false</c> otherwise.</returns>
        /// <param name="modGUID">Mod GUID.</param>
        public static bool IsModLoaded(string modGUID)
        {
            if (IsModPresent(modGUID))
            {
                BaseUnityPlugin plugin = BepInEx.Bootstrap.Chainloader.PluginInfos[modGUID]?.Instance;
                if (plugin != null)
                {
                    return true;
                }
            }
            return false;
        }

        #region ModMenu
        /// <summary>
        /// ModMenu's plugin GUID.
        /// </summary>
        public static string ModMenuGUID = "no.mrgentle.plugins.llb.modmenu";
        private static BaseUnityPlugin modmenuInstance = null;
        private static MethodInfo registerModMethod = null;
        private static MethodInfo inModOptionsMethod = null;
        private static bool lockLoadedState = false;
        private static bool isModMenuLoaded = false;

        private static bool IsModMenuLoaded()
        {
            if (lockLoadedState)
            {
                return isModMenuLoaded;
            }
            isModMenuLoaded = false;
            if (modmenuInstance != null)
            {
                isModMenuLoaded = true;
            }
            else if (IsModLoaded(ModMenuGUID))
            {
                modmenuInstance = BepInEx.Bootstrap.Chainloader.PluginInfos[ModMenuGUID].Instance;
                registerModMethod = AccessTools.Method(modmenuInstance.GetType(), "RegisterMod", new Type[] { typeof(PluginInfo), typeof(List<string>) });
                inModOptionsMethod = AccessTools.Method(modmenuInstance.GetType(), "InModOptions", new Type[] { });
                isModMenuLoaded = true;
            }
            if (LLBMLPlugin.StartReached)
            {
                lockLoadedState = true;
            }
            return isModMenuLoaded;
        }

        /// <summary>
        /// Calls the eponymous method from ModMenu, if it is loaded.
        /// </summary>
        /// <param name="pluginInfo">The Plugin's Info.</param>
        /// <param name="modmenuText">The informational text that can be displayed in the configuration menu.</param>
        public static void RegisterToModMenu(PluginInfo pluginInfo, List<string> modmenuText = null)
        {
            string modName = pluginInfo.Metadata.Name;

            if (IsModMenuLoaded())
            {
                try
                {
                    registerModMethod.Invoke(modmenuInstance, new object[] { pluginInfo, modmenuText });
                    Logger.LogDebug("Registered "+ modName +" to ModMenu.");

                }
                catch (Exception e)
                {
                    Logger.LogWarning("Caught exception: " + e.Message);
                    Logger.LogWarning("ModMenu should be loaded, but RegisterToModMenu threw.");
                    Logger.LogWarning($"Aborting Registration for {modName}.");
                }
            }
            else
            {
                Logger.LogWarning($"ModMenu is not loaded. {modName} can't register.");
                return;
            }
        }


        /// <summary>
        /// Calls the eponymous method from ModMenu, if it is loaded.
        /// </summary>
        /// <returns><c>true</c>, if ModMenu was loaded and the ModOptions screen is visible, <c>false</c> otherwise.</returns>
        public static bool InModOptions()
        {
            if (IsModMenuLoaded())
            {
                try
                {
                    return (bool)inModOptionsMethod.Invoke(modmenuInstance, new object[] { });
                }
                catch (Exception e)
                {
                    Logger.LogWarning("Caught exception: " + e.Message);
                    Logger.LogWarning("ModMenu should be loaded, but InModOptions threw.");
                }
            }
            return false;
        }
        #endregion
    }
}
