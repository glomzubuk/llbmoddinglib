﻿using System;
using System.Collections.Generic;
using BepInEx.Logging;

namespace LLBML.Utils
{
    /// <summary>
    /// A class to store and retreive <typeparamref name="T" />
    /// </summary>
    public abstract class GenericCache<K,T>
    {
        /// 
        protected static ManualLogSource Logger => LLBMLPlugin.Log;

        /// <summary>
        /// The cache.
        /// </summary>
        protected Dictionary<K, List<T>> cache = new Dictionary<K, List<T>>();

        /// <summary>
        /// The number of <typeparamref name="T" /> currently stored in the cache
        /// </summary>
        public int Count { get { return cache.Count; } }

        /// <summary>
        /// Empties the cache
        /// </summary>
        public virtual void Clear()
        {
            cache.Clear();
        }

        /// <summary>
        /// Check if the cache contains the key
        /// </summary>
        /// <returns><c>true</c>, if key is present, <c>false</c> otherwise.</returns>
        /// <param name="key">The key.</param>
        public virtual bool ContainsKey(K key)
        {
            return cache.ContainsKey(key);
        }

        /// <summary>
        /// Adds the specified key and value to the cache.
        /// </summary>
        /// <param name="key">The key to insert into.</param>
        /// <param name="value">The value to insert.</param>
        public virtual void Add(K key, T value)
        {
            if (!cache.ContainsKey(key))
                cache.Add(key, new List<T>());
            if (!cache[key].Contains(value))
                cache[key].Add(value);
        }
        /// <summary>
        /// Gets the <see cref="List{T}"/> with the specified key.
        /// </summary>
        /// <param name="key">Key.</param>
        public virtual List<T> this[K key]
        {
            get { return this.GetEntries(key); }
        }

        /// <summary>
        /// Gets the <see cref="List{T}"/> with the specified key.
        /// </summary>
        /// <param name="key">The chache key</param>
        protected virtual List<T> GetEntries(K key)
        {
            if (cache.ContainsKey(key))
                return cache[key];
            throw new KeyNotFoundException("No asset list with the specified key: " + key);
        }


        /// <summary>
        /// Gets the first cached entry with the specified key.
        /// </summary>
        /// <param name="key">The list index</param>
        protected virtual T GetEntry(K key)
        {
            return this.GetEntries(key)[0];
        }

        /// 
        override public string ToString()
        {
            string result = "[\n";
            foreach (K key in cache.Keys)
            {
                result += "key: " + key + "\n";
                foreach (T entry in cache[key])
                {
                    result += "  - " + entry + "\n";
                }
                result += "---------\n";
            }
            result += "]";
            return result;
        }
    }
}
