using System;

namespace LLBML.Utils
{
    /// <summary>
    /// Utils for getting OS information.
    /// </summary>
    public static class OSUtils
    {
        /// <summary>
        /// Whether or not the running OS is Linux-based.
        /// </summary>
        /// <returns><c>true</c> if OS is Linux-based, <c>false</c> otherwise.</returns>
        public static bool IsLinux() {
            int p = (int) Environment.OSVersion.Platform;
            return (p == 4) || (p == 6) || (p == 128);
        }
    }
}
