﻿using System;
namespace LLBML.Utils
{
    public interface IByteable
    {
        byte[] ToBytes();
    }
}
