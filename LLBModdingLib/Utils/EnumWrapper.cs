﻿using System;
namespace LLBML.Utils
{
    /// <summary>
    /// A wrapper helper to the game's obfuscated enums
    /// </summary>
    public abstract class EnumWrapper<T> where T : System.Enum
    {
#pragma warning disable 1591
        protected readonly int id;
        protected EnumWrapper(int id)
        {
            this.id = id;
        }

        public override bool Equals(object obj)
        {
            return (obj is EnumWrapper<T> || obj is T) && Equals((int)obj, (int)this);
        }
        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
        public override string ToString()
        {
            return this.id.ToString();
        }

        public static explicit operator int(EnumWrapper<T> ew) => ew.id;

        public static bool Equals(EnumWrapper<T> a, EnumWrapper<T> b) => a.id == b.id;
        public static bool operator ==(EnumWrapper<T> a, EnumWrapper<T> b) => Equals(a, b);

        public static bool NotEquals(EnumWrapper<T> a, EnumWrapper<T> b) => !Equals(a, b);
        public static bool operator !=(EnumWrapper<T> a, EnumWrapper<T> b) => NotEquals(a, b);
#pragma warning restore 1591
        }
    }
