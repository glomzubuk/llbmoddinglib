﻿using System;
using UnityEngine;

namespace LLBML.Audio
{

    /// <summary>
    /// Holds an audio clip with the infos used to load it
    /// </summary>
    public class AudioAsset
    {
        /// <summary>
        /// The <see cref="AudioClip"/>
        /// </summary>
        public AudioClip audioClip;
        /// <summary>
        /// The file information for the AudioClip
        /// </summary>
        public AudioInfo audioInfo;

        /// <summary>
        /// Initializes a new instance of <see cref="AudioAsset"/>.
        /// </summary>
        /// <param name="audioClip">Audio clip.</param>
        /// <param name="audioInfo">Audio info.</param>
        public AudioAsset(AudioClip audioClip, AudioInfo audioInfo = default(AudioInfo))
        {
            this.audioClip = audioClip;
            this.audioInfo = audioInfo;
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:LLBML.Audio.AudioAsset"/>.
        /// </summary>
        /// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:LLBML.Audio.AudioAsset"/>.</returns>
        public override string ToString()
        {
            return audioClip.name;
        }
    }
}
