﻿using System;
using System.IO;
using UnityEngine;
using BepInEx.Logging;

namespace LLBML.Audio
{
    /// <summary>
    /// A class to gather audio related data before loading it
    /// </summary>
    public struct AudioInfo
    {
        private static readonly ManualLogSource Logger = LLBMLPlugin.Log;
        private const string audioLogPrefix = "[Audio]: ";

        /// <summary>
        /// The audio file location
        /// </summary>
        public FileInfo file;
        /// <summary>
        /// The type of the audio file
        /// </summary>
        public AudioType type;
        /// <summary>
        /// The name the resulting clip could have
        /// </summary>
        public string clipNameHint;
        /// <summary>
        /// How loud the resulting clip should be
        /// </summary>
        public float volume;
        /// <summary>
        /// How the resulting clip should loop when played
        /// </summary>
        public Vector2 loopData;

        /// <param name="fileInfo">The path of the audio file</param>
        /// <param name="audioType">The type of the audio file</param>
        /// <param name="clipNameHint">The name the clip should have when loaded</param>
        /// <param name="volume">The volume of the audio file</param>
        /// <param name="loopData">The start and end point of looping, in milliseconds</param>
        public AudioInfo(FileInfo fileInfo = null, AudioType audioType = AudioType.UNKNOWN, string clipNameHint = null, float volume = 1f, Vector2 loopData = default(Vector2))
        {
            this.file = fileInfo;
            this.type = audioType;
            this.clipNameHint = clipNameHint;
            this.volume = volume;
            this.loopData = loopData;
        }

        /// <summary>
        /// Builds an <see cref="AudioInfo"/> from what can be gathered from the file
        /// </summary>
        /// <param name="file">The path of the audio file</param>
        /// <param name="clipNameHint">Uses this name instead of gathering it from the file name</param>
        /// <returns>The resulting <see cref="AudioInfo"/></returns>
        public static AudioInfo GetAudioInfo(FileInfo file, string clipNameHint = null)
        {
            AudioType audioType = AudioType.UNKNOWN;
            switch (file.Extension.ToLower())
            {
                case ".ogg": audioType = AudioType.OGGVORBIS; break;
                case ".wav": audioType = AudioType.WAV; break;
                case ".mp3": audioType = AudioType.MPEG; break;
                case ".aif": audioType = AudioType.AIFF; break;
                default:
                    Logger.LogWarning(audioLogPrefix + "Unsupported audio file encountered: " + file.FullName);
                    //throw new NotSupportedException("Unsupported audio file encountered: " + file.FullName);
                    break;
            }
            string fileBaseName = Path.GetFileNameWithoutExtension(file.FullName);
            string[] fileNameData = fileBaseName.Split(':');
            clipNameHint = clipNameHint ?? fileNameData[0];

            float volume = 1f;
            if (fileNameData.Length >= 2)
            {
                Logger.LogDebug(audioLogPrefix + $"Volume data parsed as: {fileNameData[0]} # {fileNameData[1]}");
                try
                {
                    volume = float.Parse(fileNameData[1]);
                }
                catch (Exception e)
                {
                    Logger.LogWarning(audioLogPrefix + "Exception caught trying to parse volume from " + file.FullName +
                        ", defaulting to default volume\n" + e.Message);
                }
            }


            Vector2 loopData = default(Vector2);
            if (fileNameData.Length >= 4){
                Logger.LogDebug(audioLogPrefix + $"Loop data parsed as: {fileNameData[0]} # {fileNameData[2]} # {fileNameData[3]}");
                try
                {
                    loopData = new Vector2(float.Parse(fileNameData[2]), float.Parse(fileNameData[3]));
                }
                catch (Exception e)
                {
                    Logger.LogWarning(audioLogPrefix + "Exception caught trying to parse loopdata from " + file.FullName +
                        ", defaulting to default loopdata\n"+ e.Message);
                }
            }
            return new AudioInfo(file, audioType, clipNameHint, volume, loopData);
        }
        /// <inheritdoc cref="GetAudioInfo(FileInfo, string)"/>
        /// <param name="filePath">The path of the audio file</param>
        /// <param name="clipNameHint">Uses this name instead of gathering it from the file name</param>
        public static AudioInfo GetAudioInfo(string filePath, string clipNameHint = null) => GetAudioInfo(new FileInfo(filePath), clipNameHint);
    }
}
