﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using BepInEx;
using BepInEx.Logging;
using LLBML.Utils;

namespace LLBML.Audio
{
    /// <summary>
    /// A class to load, store and retreive <see cref="AudioClip"/>
    /// </summary>
    public class AudioCache : GenericCache<string, AudioAsset>
    {
        private int runningLoadingCoroutines = 0;
        private const string audioLogPrefix = "[Audio]: ";

        /// <summary>
        /// Whether or not some loading coroutines are currently running
        /// </summary>
        public bool IsLoading { get { return runningLoadingCoroutines > 0; } }


        /// <inheritdoc cref="LoadClip(string, FileInfo, string)"/>
        /// <param name="key">The index of the resulting clip</param>
        /// <param name="filePath">The path to the audio file to load.</param>
        /// <param name="clipName">The name the resulting clip should have.</param>
        public void LoadClip(string key, string filePath, string clipName = null)
        {
            LoadClip(filePath, new FileInfo(filePath), clipName);
        }
        /// <summary>
        /// Loads the audio file, into a clip, then store it into the cache at the specified index.
        /// </summary>
        /// <param name="key">The index of the resulting clip</param>
        /// <param name="file">The path to the audio file to load.</param>
        /// <param name="clipName">The name the resulting clip should have.</param>
        public void LoadClip(string key, FileInfo file, string clipName = null)
        {
            LoadClip(key, AudioInfo.GetAudioInfo(file, clipName));
        }
        /// <summary>
        /// Loads the audio file described by the <see cref="AudioInfo"/> into a clip, then store it into the cache at the specified index.
        /// </summary>
        /// <param name="key">The index of the resulting clip</param>
        /// <param name="audioInfo">The <see cref="AudioInfo"/> of the audio file to load.</param>
        public void LoadClip(string key, AudioInfo audioInfo)
        {
            LLBMLPlugin.Instance.StartCoroutine(CLoadClip(key, audioInfo));
        }

        /// <summary>
        /// Coroutine that loads an audio file and stores it into the cache
        /// </summary>
        /// <param name="key">The index for the audio clip</param>
        /// <param name="audioInfo">The <see cref="AudioInfo"/> to load</param>
        public IEnumerator CLoadClip(string key, AudioInfo audioInfo)
        {
            runningLoadingCoroutines++;
            string clipName = audioInfo.clipNameHint ?? key;

            //Logger.LogDebug("Loading clip at: " + audioInfo.file.FullName);

            using (UnityWebRequest uwr = UnityWebRequestMultimedia.GetAudioClip(Utility.ConvertToWWWFormat(audioInfo.file.FullName), audioInfo.type))
            {
                UnityWebRequestAsyncOperation asyncOperation = null;
                try
                {
                    asyncOperation = uwr.SendWebRequest();
                }
                catch (Exception e)
                {
                    Logger.LogError("Exception caught: ");
                    Logger.LogError(e);
                    yield break;
                }
                yield return asyncOperation;
                while (!uwr.isDone)
                    yield return null;
                AudioClip audioClip;
                try
                {
                    audioClip = DownloadHandlerAudioClip.GetContent(uwr);
                }
                catch (Exception e)
                {
                    Logger.LogError(audioLogPrefix +
                                    "Failed trying to retrieve the AudioClip from UWR.\n" +
                                    $"Path: {audioInfo.file.FullName} .\n" +
                                    $"File type: {audioInfo.type} .\n" +
                                    $"Gathered Loop data : {audioInfo.loopData} .\n" +
                                    "Exception: " + e);
                    Logger.LogError(audioLogPrefix + e);

                    yield break;
                }
                if (audioClip == null)
                {
                    Logger.LogError(audioLogPrefix + "Audioclip " + key + " was null");
                }
                else
                {
                    audioClip.name = clipName;
                    this.Add(key, new AudioAsset(audioClip, audioInfo));
                }
            }
            runningLoadingCoroutines--;
        }

        /// <summary>
        /// Adds the specified key and clip.
        /// </summary>
        /// <param name="key">The key to insert.</param>
        /// <param name="audioClip">The <see cref="AudioClip"/>.</param>
        public void Add(string key, AudioClip audioClip)
        {
            base.Add(key, new AudioAsset(audioClip));
        }

        /// <summary>
        /// Adds the specified key and asset.
        /// </summary>
        /// <param name="key">The key to insert into.</param>
        /// <param name="audioAsset">The <see cref="AudioAsset"/> to insert.</param>
        public override void Add(string key, AudioAsset audioAsset)
        {
            base.Add(key, audioAsset);
        }

        /// <summary>
        /// Gets a list of <see cref="AudioClip"/> under the assets with the specified key.
        /// </summary>
        /// <param name="key">The list index</param>
        public List<AudioClip> GetClips(string key)
        {
            return cache[key].ConvertAll<AudioClip>((audioAsset) => audioAsset.audioClip);
        }
        /// <summary>
        /// Gets the first clip that is found under the assets with the specified key.
        /// </summary>
        /// <param name="key">The list index</param>
        public AudioClip GetClip(string key)
        {
            return this.GetClips(key)[0];
        }

        /// <summary>
        /// Gets the <see cref="List{AudioAsset}"/> with the specified key.
        /// </summary>
        /// <param name="key">Key.</param>
        public List<AudioAsset> GetAssets(string key)
        {
            return cache[key];
        }

        /// <summary>
        /// Gets the first <see cref="AudioAsset"/> that is found with the specified key.
        /// </summary>
        /// <param name="key">The list index</param>
        public AudioAsset GetAsset(string key)
        {
            return this.GetAssets(key)[0];
        }
    }
}
