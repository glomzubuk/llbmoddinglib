﻿using System;
using GameplayEntities;
using LLHandlers;

namespace LLBML
{
    public static class BallApi
    {
        /// <summary>
        /// Whether or not there's no ball in match.
        /// </summary>
        /// <returns><c>true</c> if no balls are in match, <c>false</c> otherwise.</returns>
        public static bool NoBallsActivelyInMatch()
        {
            if (BallHandler.instance == null)
                return true;
            else
                return BallHandler.instance.NoBallsActivelyInMatch();
        }

        /// <summary>
        /// Whether or not there's at least one ball in match.
        /// </summary>
        /// <returns><c>true</c> if a balls is in match, <c>false</c> otherwise.</returns>
        public static bool BallsActivelyInMatch()
        {
            if (BallHandler.instance == null)
                return false;
            else
            {
                foreach (BallEntity ballEntity in BallHandler.instance.balls)
                {
                    if (ballEntity.IsActivelyInMatch())
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public static BallEntity GetBall(int ballIndex = 0)
        {
            if (BallHandler.instance == null)
                return null;
            else
                return BallHandler.instance.GetBall();
        }
    }
}
