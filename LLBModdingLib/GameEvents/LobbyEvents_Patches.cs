﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection.Emit;
using UnityEngine;
using Multiplayer;
using HarmonyLib;
using LLBML.States;
using LLBML.Players;
using LLBML.Utils;
using Steamworks;

namespace LLBML.GameEvents
{
#pragma warning disable 1591
    internal static class LobbyEvents_Patches
    {
        [HarmonyPatch(typeof(HDLIJDBFGKN), nameof(HDLIJDBFGKN.OAACLLGMFLH), new Type[] {})]
        [HarmonyTranspiler]
        public static IEnumerable<CodeInstruction> StartGameHost_Transpiler(IEnumerable<CodeInstruction> instructions, ILGenerator iL)
        {
            CodeMatcher cm = new CodeMatcher(instructions, iL);
            PatchUtils.LogInstructions(cm.Instructions(), 252,264);
            cm.MatchForward(true, // false = move at the start of the match, true = move at the end of the match
                new CodeMatch(OpCodes.Blt),
                new CodeMatch(OpCodes.Call),
                new CodeMatch(OpCodes.Callvirt),
                new CodeMatch(OpCodes.Ldc_I4),
                new CodeMatch(OpCodes.Ldc_I4_M1),
                new CodeMatch(OpCodes.Ldc_I4_M1),
                new CodeMatch(OpCodes.Ldloc_0),
                new CodeMatch(OpCodes.Ldfld));
            PatchUtils.LogInstruction(cm.Instruction);
            var field = cm.Operand;
            cm.Advance(-6);
            PatchUtils.LogInstruction(cm.Instruction);
            try
            {
                cm.InsertAndAdvance(
                    new CodeInstruction(OpCodes.Ldloc_0),
                    new CodeInstruction(OpCodes.Ldfld, field),
                    new CodeInstruction(OpCodes.Ldloc_0),
                    Transpilers.EmitDelegate<Action<HDLIJDBFGKN, int[]>>(
                    (gameState, decisions) => {
                        LobbyEvents.OnDecisionsCall(gameState, new OnDecisionsArgs(true, decisions));
                    }
                ));
            }
            catch (Exception e)
            {
                LLBMLPlugin.Log.LogInfo(e);
            }
            PatchUtils.LogInstructions(cm.Instructions(), 252,264);
            return cm.InstructionEnumeration();
        }
        [HarmonyPatch(typeof(HDLIJDBFGKN), nameof(HDLIJDBFGKN.OAACLLGMFLH), new Type[] {typeof(int[])})]
        [HarmonyPrefix]
        public static bool StartGame_Prefix(HDLIJDBFGKN __instance, int[] FHFDJMKAHCI)
        {
            LobbyEvents.OnDecisionsCall(__instance, new OnDecisionsArgs(false, FHFDJMKAHCI));
            return true;
        }


        [HarmonyPatch(typeof(HPNLMFHPHFD), nameof(HPNLMFHPHFD.KOLLNKIKIKM))]
        [HarmonyPrefix]
        public static bool OpenStageSelect_Prefix(HDLIJDBFGKN __instance, bool JFEKJLHCEFD, bool PGJKMPOJCBC, ScreenType FLMBCGMOCKC)
        {
            LobbyEvents.OnStageSelectOpenCall(__instance, new OnStageSelectOpenArgs(JFEKJLHCEFD, PGJKMPOJCBC, FLMBCGMOCKC));
            return true;
        }

        [HarmonyPatch(typeof(PlayersCharacterButton), "<Init>m__0")]
        [HarmonyPostfix]
        public static void PickChar_Postfix(int pNr, PlayersCharacterButton __instance)
        {
            LobbyEvents.OnUserCharacterPickCall(__instance, new OnUserCharacterPickArgs(pNr, __instance.character));
        }
        [HarmonyPatch(typeof(PlayersSelection), "<Init>m__4")]
        [HarmonyPostfix]
        public static void PickSkin_Postfix(int pNr, PlayersSelection __instance)
        {
            LobbyEvents.OnUserSkinClickCall(__instance, new OnUserSkinClickArgs(pNr, __instance.playerNr));
        }


        private static CSteamID lobby_CSteamID;
        [HarmonyPatch(typeof(KKMGLMJABKH), nameof(KKMGLMJABKH.OMANHANHHKC))] // PlatformSteam.OnLobbyEntered
        [HarmonyPostfix]
        public static void OnLobbyEntered_Postfix(KMILCAPIEHH PKEOMLCLPNE)
        {
            lobby_CSteamID = new CSteamID(PKEOMLCLPNE.CDPLMFKJIDK);
        }

        [HarmonyPostfix]
        [HarmonyPatch(typeof(HPNLMFHPHFD), nameof(HPNLMFHPHFD.CNewState))] // GameStatesLobby.CNewState
        public static IEnumerator CNewState_Postfix(IEnumerator orig, HPNLMFHPHFD __instance)
        {
            for (int c = 1; orig.MoveNext(); c++)
            {
                // if c == some number of steps, run your stuff
                yield return orig.Current; // Run original enumerator for one step
            }

            LobbyEventArgs args;

            /*LLBMLPlugin.Log.LogDebug(
                $"IsInOnlineLobby: {GameStates.IsInOnlineLobby()}\n" +
                $"GetCurrent: {(GameState)DNPFJHMAIBP.HHMOGKIMBNM()}\n" +
                $"GameStatesLobbyOnline : {__instance is HDLIJDBFGKN}");*/
            if (GameStates.IsInOnlineLobby())
            {
                args = new LobbyEventArgs(true, lobby_CSteamID.ToString(), Player.GetPlayer(0)?.peer?.peerId); // PlatformSteam.curLobby
            }
            else
            {
                args = new LobbyEventArgs(false, null, null);
            }
            LobbyEvents.OnLobbyEnteredCall(__instance, args);
            LLBMLPlugin.Instance.StartCoroutine(CallLobbyReady(__instance, args));
        }
        public static IEnumerator CallLobbyReady(HPNLMFHPHFD instance, LobbyEventArgs args)
        {
            yield return new WaitForSeconds(0.2f);
            if (GameStates.IsInLobby())
            {
                LobbyEvents.OnLobbyReadyCall(instance, new LobbyReadyArgs(args.isOnline, args.lobby_id, args.host_id ?? Player.GetPlayer(0)?.peer?.peerId));
            }
        }


        [HarmonyPatch(typeof(ALDOKEMAOMB), nameof(ALDOKEMAOMB.EKNFACPOJCM))] // Player.JoinMatch
        [HarmonyPostfix]
        public static void JoinMatch_Postfix(bool __0, ALDOKEMAOMB __instance)
        {
            bool join = __0;
            Player player = __instance;
            LobbyEvents.OnPlayerJoinCall(__instance, new OnPlayerJoinArgs(player.nr, player.isLocal));
        }

        [HarmonyPatch(typeof(Peer), nameof(Peer.UnlinkFromPlayer))]
        [HarmonyPostfix]
        public static void UnlinkFromPlayer_Postfix(Peer __instance)
        {
            LobbyEvents.OnUnlinkFromPlayerCall(__instance, new OnUnlinkFromPlayerArgs(__instance.playerNr));
        }
    }
#pragma warning restore 1591
}
