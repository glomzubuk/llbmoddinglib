﻿using System;
using HarmonyLib;
using LLBML.States;

namespace LLBML.GameEvents
{
    /// <summary>
    /// Delegate for the <see cref="MenuEvents.OnMainMenuFirstEntered"/> event.
    /// </summary>
    public delegate void OnMainMenuFirstEnteredHandler(object source, OnMainMenuEnteredArgs e);
    /// <summary>
    /// Delegate for the <see cref="MenuEvents.OnMainMenuEntered"/> event.
    /// </summary>
    public delegate void OnMainMenuEnteredHandler(object source, OnMainMenuEnteredArgs e);

    /// <summary>
    /// A class to hold various events related to lobby activity.
    /// </summary>
    public class MenuEvents
    {
        internal static void Patch(Harmony harmonyInstance)
        {
            harmonyInstance.PatchAll(typeof(MenuEvents_Patches));
            OnMainMenuFirstEntered += (o, a) =>
                LLBMLPlugin.Log.LogDebug("Event OnMainMenuFirstEntered triggered. Previous state: " + a.previousState);
            OnMainMenuEntered += (o, a) =>
                LLBMLPlugin.Log.LogDebug("Event OnMainMenuEntered triggered. Previous state: " + a.previousState);
        }

        /// <summary>
        /// Occurs when the main menu is entered for the first time since the game's launch.
        /// </summary>
        public static event OnMainMenuFirstEnteredHandler OnMainMenuFirstEntered;
        internal static void OnMainMenuFirstEnteredCall(object source, OnMainMenuEnteredArgs e)
        {
            if (OnMainMenuFirstEntered == null) return;
            OnMainMenuFirstEntered(source, e);
        }


        /// <summary>
        /// Occurs when the main menu is entered.
        /// </summary>
        public static event OnMainMenuEnteredHandler OnMainMenuEntered;
        internal static void OnMainMenuEnteredCall(object source, OnMainMenuEnteredArgs e)
        {
            if (OnMainMenuEntered == null) return;
            OnMainMenuEntered(source, e);
        }
    }

    /// <summary>
    /// Class to hold arguments for the <see cref="MenuEvents.OnMainMenuEntered"/> event.
    /// </summary>
    public class OnMainMenuEnteredArgs : EventArgs
    {
        /// <summary>
        /// The character picked by the user.
        /// </summary>
        public GameState previousState { get; private set; }

        public OnMainMenuEnteredArgs(GameState previousState)
        {
            this.previousState = previousState;
        }
    }
}
