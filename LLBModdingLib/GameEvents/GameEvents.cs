﻿using System;
using HarmonyLib;


namespace LLBML.GameEvents
{
    internal static class GameEvents
    {
        internal static void PatchAll(Harmony harmonyInstance)
        {
            GameStateEvents.Patch(harmonyInstance);
            LobbyEvents.Patch(harmonyInstance);
            MenuEvents.Patch(harmonyInstance);
        }
    }
}
