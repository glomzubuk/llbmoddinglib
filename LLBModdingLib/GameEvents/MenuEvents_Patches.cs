﻿using System;
using LLScreen;
using HarmonyLib;
using LLBML.States;


namespace LLBML.GameEvents
{
#pragma warning disable 1591
    internal static class MenuEvents_Patches
    {
        private static bool firstSinceStart = true;
        [HarmonyPatch(typeof(IOGKKINMEFB), nameof(IOGKKINMEFB.CNewState))]
        [HarmonyPostfix]
        public static void CNewState_Postfix(JOFJHDJHJGI __0, JOFJHDJHJGI __1, IOGKKINMEFB __instance)
        {
            if (__1 == GameState.MENU)
            {
                if (firstSinceStart)
                {
                    MenuEvents.OnMainMenuFirstEnteredCall(__instance, new OnMainMenuEnteredArgs(__0));
                    firstSinceStart = false;
                }
                MenuEvents.OnMainMenuEnteredCall(__instance, new OnMainMenuEnteredArgs(__0));
            }
        }
    }
#pragma warning restore 1591
}
