﻿using System;
using System.Collections.Generic;
using BepInEx;
using UnityEngine;
using LLScreen;
using LLHandlers;
using LLBML.States;

namespace LLBML
{
    public static class LoadingScreen
    {
        private static Dictionary<string, LoadingInfo> loadingInfos = new Dictionary<string, LoadingInfo>();
        public static bool AnyModLoading { get { return loadingInfos.Count > 0; } }

        public static void SetLoading(PluginInfo plugin, bool loading, string message = null, bool showScreen = true)
        {
            if (loading)
            {
                if (!loadingInfos.ContainsKey(plugin.Metadata.GUID))
                {
                    string loadingMessage;
                    if (message != null) loadingMessage = message;
                    else loadingMessage = plugin.Metadata.Name + " is loading external resources";

                    loadingInfos.Add(plugin.Metadata.GUID, new LoadingInfo(loadingMessage, showScreen));
                }
            }
            else
            {
                if (loadingInfos.ContainsKey(plugin.Metadata.GUID))
                {
                    loadingInfos.Remove(plugin.Metadata.GUID);
                }
            }
            UpdateState();
        }

        public static void UpdateState()
        {
            if (AnyModLoading)
            {
                if (!UIScreen.loadingScreenActive)
                {
                    UIScreen.SetLoadingScreen(true, false, false, Stage.NONE);
                }
            }
            else
            {
                if (UIScreen.loadingScreenActive && GameStates.GetCurrent() != States.GameState.NONE)
                {
                    UIScreen.SetLoadingScreen(false, false, false, Stage.NONE);
                }
            }
        }


        internal static void OnGUI()
        {
            if (UIScreen.loadingScreenActive && AnyModLoading)
            {
                var originalColor = GUI.contentColor;
                var originalLabelFontSize = GUI.skin.label.fontSize;
                var originalLabelAlignment = GUI.skin.label.alignment;

                GUI.contentColor = Color.white;
                GUI.skin.label.fontSize = 50;
                GUIStyle label = new GUIStyle(GUI.skin.label);
                GUI.skin.label.alignment = TextAnchor.MiddleCenter;

                float labelY = UIScreen.GetResolutionFromConfig().height / loadingInfos.Count;

                foreach (LoadingInfo loadingInfo in loadingInfos.Values)
                {
                    GUI.Label(new Rect(0, labelY - 25, Screen.width, 50), loadingInfo.message);
                    labelY -= 50;
                }

                GUI.skin.label.alignment = TextAnchor.MiddleLeft;
                GUI.contentColor = originalColor;
                GUI.skin.label.fontSize = originalLabelFontSize;
                GUI.skin.label.alignment = originalLabelAlignment;
            }
        }
    }

    public struct LoadingInfo
    {
        public string message;
        public bool showScreen;
        public LoadingInfo(string _message, bool _showScreen)
        {
            message = _message;
            showScreen = _showScreen;
        }
    }
}
