﻿using System;
using LLHandlers;
using LLBML.Players;


namespace LLBML.Settings
{
    public class GameSettings
    {
#pragma warning disable 1591
        private readonly JOMBNFKIHIC _gameSettings;

        public GameSettings(JOMBNFKIHIC gs)
        {
            this._gameSettings = gs;
        }
        public override bool Equals(object obj) => (obj is JOMBNFKIHIC || obj is GameSettings) && Equals((JOMBNFKIHIC)obj, (JOMBNFKIHIC)this);
        public override int GetHashCode() => _gameSettings.GetHashCode();
        public override string ToString() => _gameSettings.ToString();
        public static implicit operator JOMBNFKIHIC(GameSettings gs) => gs._gameSettings;
        public static implicit operator GameSettings(JOMBNFKIHIC gs) => new GameSettings(gs);


        #region wrapper
        public static OnlineMode OnlineMode { get => JOMBNFKIHIC.EAENFOJNNGP; set => JOMBNFKIHIC.EAENFOJNNGP = value; }
        public static bool IsOnline { get => JOMBNFKIHIC.GDNFJCCCKDM; }

        public static GameSettings current { get => JOMBNFKIHIC.GIGAKBJGFDI; set => JOMBNFKIHIC.GIGAKBJGFDI = value; }


        public PowerupSelection PowerupSelection { get => _gameSettings.BFLJHKPDNKO; set => _gameSettings.BFLJHKPDNKO = value; }
        public bool BallTagging { get => _gameSettings.BNGBHJDEJNC; set => _gameSettings.BNGBHJDEJNC = value; }
        public int MinSpeed { get => _gameSettings.KHOEFMFHDMC; set => _gameSettings.KHOEFMFHDMC = value; }
        public bool UsePoints { get => _gameSettings.KOBEJOIALMO; set => _gameSettings.KOBEJOIALMO = value; }
        public bool SinglePowerup { get => _gameSettings.OKIDIKJPCEA; }

        public Stage stage { get => _gameSettings.OOEPDFABFIP; set => _gameSettings.OOEPDFABFIP = value; }
        public StageRandom stageRandom { get => _gameSettings.OCPOONFEMCA; set => _gameSettings.OCPOONFEMCA = value; }
        public int stocks { get => _gameSettings.MLEKMMGIFMF; set => _gameSettings.MLEKMMGIFMF = value; }
        public int points { get => _gameSettings.HGHFBMLJGEM; set => _gameSettings.HGHFBMLJGEM = value; }
        public bool pointInfinite { get => _gameSettings.LDEAKMILLHE; set => _gameSettings.LDEAKMILLHE = value; }
        public bool mUsePoints { get => _gameSettings.FONKOHIGBLE; set => _gameSettings.FONKOHIGBLE = value; }
        public int time { get => _gameSettings.GKJAGGKNFCO; set => _gameSettings.GKJAGGKNFCO = value; }
        public bool timeInfinite { get => _gameSettings.BLADHBMDPPK; set => _gameSettings.BLADHBMDPPK = value; }
        public bool mBallTagging { get => _gameSettings.NAKDBIFCJDI; set => _gameSettings.NAKDBIFCJDI = value; }
        public int mMinSpeed { get => _gameSettings.HMOBHEIJCLM; set => _gameSettings.HMOBHEIJCLM = value; }
        public int energy { get => _gameSettings.IKGAIFLLLAG; set => _gameSettings.IKGAIFLLLAG = value; }
        public HpFactor useHP { get => _gameSettings.BKKMIBGLAEC; set => _gameSettings.BKKMIBGLAEC = value; }
        public PowerupSelection havePowerups { get => _gameSettings.JJLDLMPOEEI; set => _gameSettings.JJLDLMPOEEI = value; }
        public BallType ballType { get => _gameSettings.HKOFJKJDPGL; set => _gameSettings.HKOFJKJDPGL = value; }

        #endregion wrapper
#pragma warning restore 1591

        #region additions
        #endregion additions
    }
}
