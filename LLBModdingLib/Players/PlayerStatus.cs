﻿using System;
using LLBML.Utils;

namespace LLBML.Players
{
    /// <summary>
    /// A wrapper to the game's obfuscated Team enum
    /// </summary>
    public class PlayerStatus : EnumWrapper<ALDOKEMAOMB.AKNMBDLMNJM>
    {
#pragma warning disable 1591
        PlayerStatus(int id) : base(id) { }
        PlayerStatus(ALDOKEMAOMB.AKNMBDLMNJM team) : base((int)team) { }

        public override string ToString()
        {
            return ((Enum)this.id).ToString();
        }

        public static implicit operator ALDOKEMAOMB.AKNMBDLMNJM(PlayerStatus ew) => (ALDOKEMAOMB.AKNMBDLMNJM)ew.id;
        public static implicit operator PlayerStatus(int id) => new PlayerStatus(id);
        public static implicit operator Enum(PlayerStatus ew) => (Enum)ew.id;
        public static implicit operator PlayerStatus(Enum val) => new PlayerStatus((int)val);
        public static implicit operator PlayerStatus(ALDOKEMAOMB.AKNMBDLMNJM val) => new PlayerStatus(val);

        #region wrapper
        public enum Enum
        {
            NONE,
            IN_MATCH,
            DISCONNECTED,
            SPECTATOR
        }
        public enum Enum_Mappings
        {
            NONE = ALDOKEMAOMB.AKNMBDLMNJM.NMJDMHNMDNJ,
            IN_MATCH = ALDOKEMAOMB.AKNMBDLMNJM.KDJGKNGMLNO,
            DISCONNECTED = ALDOKEMAOMB.AKNMBDLMNJM.HHIDHJKLLHK,
            SPECTATOR = ALDOKEMAOMB.AKNMBDLMNJM.BBDAPCHJPNI,
        }

        public static readonly PlayerStatus NONE = Enum.NONE;
        public static readonly PlayerStatus IN_MATCH = Enum.IN_MATCH;
        public static readonly PlayerStatus DISCONNECTED = Enum.DISCONNECTED;
        public static readonly PlayerStatus SPECTATOR = Enum.SPECTATOR;
        #endregion
#pragma warning restore 1591
    }
}
