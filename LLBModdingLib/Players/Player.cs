﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using GameplayEntities;
using LLHandlers;
using LLGUI;
using Multiplayer;

namespace LLBML.Players
{
    /// <summary>
    /// A wrapper to the game's obfuscated Player class
    /// </summary>
    public class Player
    {
#pragma warning disable 1591
        private readonly ALDOKEMAOMB _player;

        public Player(ALDOKEMAOMB p)
        {
            this._player = p;
        }
        public override bool Equals(object obj) => (obj is ALDOKEMAOMB || obj is Player) && Equals((ALDOKEMAOMB)obj, (ALDOKEMAOMB)this);
        public override int GetHashCode() => _player.GetHashCode();
        public override string ToString(){
            return $"P{nr} ({playerStatus})";
        }
        public static implicit operator ALDOKEMAOMB(Player p) => p._player;
        public static implicit operator Player(ALDOKEMAOMB p) => new Player(p);

        #region wrapper
        public Character Character { get => _player.LALEEFJMMLH; set => _player.LALEEFJMMLH = value; }
        public CharacterVariant CharacterVariant { get => this.variant; set => this.variant = value; }
        public Character CharacterSelected { get => _player.DOFCCEDJODB; set => _player.DOFCCEDJODB = value; }
        public bool CharacterSelectedIsRandom => _player.IMJLOPPPIJM;
        public bool IsSpectator => _player.CHGMPLJHKFN;
        public bool IsLocalPeer => _player.GAFCIHKIGNM;
        public bool IsAI { get => _player.ALBOPCLADGN; set => _player.ALBOPCLADGN = value; }
        public bool IsInMatch => _player.NGLDMOLLPLK;
        public bool IsDisconnected => _player.CMCIGIEHGHO;
        public bool DidJoinedMatch => _player.PNHOIDECPJE;
        public Team Team { get => _player.HEOKEMBMDIJ; set => _player.HEOKEMBMDIJ = value; }


        public void JoinMatch(bool join) => _player.EKNFACPOJCM(join);
        public void LeaveMatch() => _player.GBMGMFIBFGA();
        public void SetSpectator() => _player.OKDEILOGKFB();
        public void Reset() => _player.FMCGJNPNIPH();
        public void ResetName() => _player.IOCOPIHGLCK();
        public void SetCursorActive(bool active, float relX = -1f, float relY = -1f) => _player.PGIPMONCFAG(active, relX, relY);
        public void UpdateLight() => _player.CFJBBPGANNF();
        public void ResetTeam(GameMode gameMode, bool changeAnyway) => _player.MLFHMGNCMNA(gameMode, changeAnyway);
        public Team GetNextTeam(GameMode gameMode, int prevNext = 1) => _player.EICBBKLGNBF(gameMode, prevNext);

        public void CheckCheats(Character character) => _player.OKNJLPBPDGC(character);
        public bool BiggerHeadActivated(Character character) => _player.ODEGEFBNONJ(character);
        public bool SmallerHeadActivated(Character character) => _player.PJLGLGEHJNN(character);

        public Character GetRandomCharacter(bool any) => _player.HGPNPNPJBMK(any);
        public Character GetRandomCharacter(Character[] skipCharacters = null) => _player.HGPNPNPJBMK(skipCharacters);

        public CharacterVariant GetVariantFirst() => _player.DIGHIEBNDPE();
        public CharacterVariant GetVariantFirst(Character forCharacter) => _player.DIGHIEBNDPE(forCharacter);
        public CharacterVariant GetVariantNext(int prevNext = 1) => _player.MKDAGHDAMML(prevNext);
        public CharacterVariant GetVariantRandom(bool any = false) => _player.GLCHOPBLHEB(any);
        public CharacterVariant GetVariantFixed() => _player.HCFKKMIPDIN();

        public void DetermineCharacter(Character[] skipCharacters = null) => _player.CHDHDGAHNPB(skipCharacters);
        public void ClearDetermine() => _player.BIOONNIPAMK();


        public int nr { get => _player.CJFLMDNNMIE; set => _player.CJFLMDNNMIE = value; }
        public string name { get => _player.COPBANHBAEE; set => _player.COPBANHBAEE = value; }
        public CharacterVariant variant { get => _player.AIINAIDBHJI; set => _player.AIINAIDBHJI = value; }
        public Controller controller { get => _player.GDEMBCKIDMA; set => _player.GDEMBCKIDMA = value; }
        public LLCursor cursor { get => _player.OBELDJGOOIJ; set => _player.OBELDJGOOIJ = value; }
        public PlayerEntity playerEntity { get => _player.JCCIAMJEODH; set => _player.JCCIAMJEODH = value; }
        public bool isLocal { get => _player.GAFCIHKIGNM; set => _player.GAFCIHKIGNM = value; }
        public string ip { get => _player.LGKJGGMLNFL; set => _player.LGKJGGMLNFL = value; }
        public int port { get => _player.MIHNOINHDJN; set => _player.MIHNOINHDJN = value; }
        public Peer peer { get => _player.KLEEADMGHNE; set => _player.KLEEADMGHNE = value; }
        public int cpuSelecting { get => _player.OLNANNOFOJO; set => _player.OLNANNOFOJO = value; }
        public bool ready { get => _player.GFCMODHPPCN; set => _player.GFCMODHPPCN = value; }
        public bool selected { get => _player.CHNGAKOIJFE; set => _player.CHNGAKOIJFE = value; }
        public int aiLevel { get => _player.INPJBIFEPMB; set => _player.INPJBIFEPMB = value; }
        public PlayerStatus playerStatus { get => _player.LIPOCPBOHBP; set => _player.LIPOCPBOHBP = value; }


        public static void Init() => ALDOKEMAOMB.GJGPFIOMNJN();
        public static void ResetAll() => ALDOKEMAOMB.OPHACFDBPHM();

        public static IEnumerable<ALDOKEMAOMB> EPlayers() => ALDOKEMAOMB.BCLFPIDGLHE();

        public static Player GetPlayer(int playerNr) => ALDOKEMAOMB.BJDPHEHJJJK(playerNr);

        public static void ForAll(Action<ALDOKEMAOMB> callback) => ALDOKEMAOMB.BCNIADLHPOH(callback);
        public static void ForAll(Action<Player> callback) => ALDOKEMAOMB.BCNIADLHPOH((ALDOKEMAOMB player) => callback(player));

        public static void ForAllInMatch(Action<ALDOKEMAOMB> callback) => ALDOKEMAOMB.ICOCPAFKCCE(callback);
        public static void ForAllInMatch(Action<Player> callback) => ALDOKEMAOMB.ICOCPAFKCCE((ALDOKEMAOMB player) => callback(player));

        public static void ForAllInMatch(Action<PlayerEntity> callback) => ALDOKEMAOMB.ICOCPAFKCCE(callback);
        public static void ForAllInTeam(Team team, Action<PlayerEntity> callback) => ALDOKEMAOMB.KPKPNDCBBOC(team, callback);


        public static void UpdateLights() => ALDOKEMAOMB.ONMJIMANMKI();
        public static string GetRandomName() => ALDOKEMAOMB.FAFALGPJHIA();

        public static int nPlayersJoinedMatch { get => ALDOKEMAOMB.OBIEGJGBIDO; set => ALDOKEMAOMB.OBIEGJGBIDO = value; }
        public static int nPlayersInMatch { get => ALDOKEMAOMB.HKOHPDFPMBK; set => ALDOKEMAOMB.HKOHPDFPMBK = value; }
        public static int MAX_PLAYERS { get => ALDOKEMAOMB.EGFMEOMNBEL; }
        #endregion wrapper
#pragma warning restore 1591

        #region additions

        /// <summary>
        /// Gets the current player number for the local player.
        /// </summary>
        /// <remarks>Curently not failsafe, game has it as -1 in some occasions, and localPeer is initialized before the Player is</remarks>
        /// <returns>The current platform as a PlatformBase object.</returns>
        public static int LocalPlayerNumber => P2P.localPeer?.playerNr ?? 0;

        /// <summary>
        /// Casts <see cref="EPlayers()"/> to a <see cref="List{Player}"/>
        /// </summary>
        /// <returns>A list of <see cref="Player"/></returns>
        public static List<Player> GetPlayerList()
        {
            //return EPlayers().Cast<Player>().ToList();
            List<Player> players = new List<Player>();
            foreach (Player player in EPlayers())
            {
                players.Add(player);
            }
            return players;
        }

        /// <summary>
        /// Casts <see cref="EPlayers()"/> to a <see cref="List{Player}"/>
        /// </summary>
        /// <returns>A list of <see cref="Player"/></returns>
        public static Player GetLocalPlayer() {
            int lpnr = LocalPlayerNumber;
            if (lpnr < 0 || lpnr > 3)
            {
                LLBMLPlugin.Log.LogWarning("Requested Local Player but local player number was: " + lpnr);
                return null;
            }
            Player localPlayer = ALDOKEMAOMB.BJDPHEHJJJK(lpnr);
            if (localPlayer == null)
            {
                LLBMLPlugin.Log.LogWarning("Requested Local Player is null");
            }
            return localPlayer;
        }

        /// <summary>
        /// Wraps <see cref="ForAllInMatch(Action{Player})"/> to additionaly check if the <see cref="Player"/> is actively in match
        /// </summary>
        /// <param name="callback">The function applying to each <see cref="Player"/></param>
        public static void ForAllActivelyInMatch(Action<Player> callback)
        {
            ForAllInMatch(delegate (Player player)
            {
                if (player.playerEntity.IsActivelyInMatch())
                    callback(player);
            });
        }

        /// <summary>
        /// Wraps <see cref="ForAllInMatch(Action{PlayerEntity})"/> to additionaly check if the <see cref="PlayerEntity"/> is actively in match
        /// </summary>
        /// <param name="callback">The function applying to each <see cref="PlayerEntity"/></param>
        public static void ForAllActivelyInMatch(Action<PlayerEntity> callback)
        {
            ForAllInMatch(delegate (PlayerEntity playerEntity)
            {
                if (playerEntity.IsActivelyInMatch())
                    callback(playerEntity);
            });
        }
        #endregion additions
    }
}
