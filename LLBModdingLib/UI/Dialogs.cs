﻿using System;
using LLGUI;
using LLScreen;

namespace LLBML.UI
{
    /// <summary>
    /// A helper class to open and manage Dialogs.
    /// </summary>
    public class Dialogs
    {
        private static ScreenBase screenDialog;

        private static void CloseDialog()
        {
            UIScreen.Close(screenDialog, ScreenTransition.NONE);
            screenDialog = null;
        }
        private static bool IsDialogOpen()
        {
            if (screenDialog != null)
            {
                return true;
            }
            foreach (ScreenBase screen in ScreenApi.CurrentScreens)
            {
                if (screen != null && (
                    screen.screenType == ScreenType.DIALOG ||
                    screen.screenType == ScreenType.DIALOG_BUSY ||
                    screen.screenType == ScreenType.DIALOG_CONFIRMATION))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Opens a dialog with customisable text, action and an optional button.
        /// </summary>
        /// <param name="title">The dialog's title.</param>
        /// <param name="message">The dialog's message.</param>
        /// <param name="buttonText">The button's label. Ommiting it will keep the button hidden</param>
        /// <param name="onClickButton">The callback that will be invoked when the button is clicked.</param>
        public static void OpenDialog(string title, string message, string buttonText = null, Action onClickButton = null)
        {
            if (IsDialogOpen())
            {
                LLBMLPlugin.Log.LogWarning("Tried to open a dialog while one was already opened. Title:" + title);
                return;
            }

            screenDialog = UIScreen.Open(ScreenType.DIALOG, 2);
            ScreenDialog dialog = screenDialog as ScreenDialog;

            dialog.SetText(title, message);

            if (buttonText != null)
            {
                dialog.ShowButton(buttonText);
            }
            else
            {
                dialog.AutoClose(10f);
            }

            LLButton btOk = dialog.btOk;
            btOk.onClick = new LLClickable.ControlDelegate(delegate (int playerNr)
            {
                if (onClickButton != null)
                {
                    onClickButton.Invoke();
                }
                CloseDialog();
            });
        }

        /// <summary>
        /// Opens a confirmation dialog with confirm and cancel buttons.
        /// </summary>
        /// <param name="title">The dialog's title.</param>
        /// <param name="text">The dialog's message.</param>
        /// <param name="onClickOK">The callback that will be invoked when the OK button is clicked.</param>
        /// <param name="onClickCancel">The callback that will be invoked when the Cancel button is clicked.</param>
        public static void OpenConfirmationDialog(string title, string text, Action onClickOK = null, Action onClickCancel = null)
        {
            if (IsDialogOpen())
            {
                LLBMLPlugin.Log.LogWarning("Tried to open a dialog while one was already opened. Title:" + title);
                return;
            }

            screenDialog = UIScreen.Open(ScreenType.DIALOG_CONFIRMATION, 2, ScreenTransition.NONE, true);
            ScreenDialogConfirmation screenDialogConfirmation = screenDialog as ScreenDialogConfirmation;

            screenDialogConfirmation.SetText(title, text, -1);

            LLButton btOk = screenDialogConfirmation.btOk;
            btOk.onClick = new LLClickable.ControlDelegate(delegate (int playerNr)
            {
                if (onClickOK != null)
                {
                    onClickOK.Invoke();
                }
                CloseDialog();
            });

            LLButton btCancel = screenDialogConfirmation.btCancel;
            btCancel.onClick = new LLClickable.ControlDelegate(delegate (int playerNr)
            {
                if (onClickCancel != null)
                {
                    onClickCancel.Invoke();
                }
                CloseDialog();
            });
        }

    }
}
