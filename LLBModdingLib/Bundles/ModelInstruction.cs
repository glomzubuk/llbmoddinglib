using LLBML.Utils;

namespace LLBML.Bundles
{
    /// <summary>
    /// A wrapper to the game's obfuscated ModelInstruction enum
    /// </summary>
    public class ModelInstruction : EnumWrapper<FKBHNEMDBMK>
    {
#pragma warning disable 1591
        ModelInstruction(int id) : base(id) { }
        ModelInstruction(FKBHNEMDBMK modelInstruction) : base((int)modelInstruction) { }

        public override string ToString() => ((Enum)this.id).ToString();


        public static implicit operator FKBHNEMDBMK(ModelInstruction ew) => (FKBHNEMDBMK)ew.id;
        public static implicit operator ModelInstruction(FKBHNEMDBMK val) => new ModelInstruction(val);
        public static implicit operator ModelInstruction(int id) => new ModelInstruction(id);
        public static implicit operator ModelInstruction(Enum val) => new ModelInstruction((int)val);
        public static implicit operator Enum(ModelInstruction ew) => (Enum)ew.id;

#region wrapper
        public enum Enum
        {
            NONE,
            OUTLINE_WHITE_ON_MESH,
            NO_SHADOW,
            SHARED_MATERIAL,
            MATERIAL_FROM_CHARACTER,
        }
        public enum Enum_Mapping
        {
            NONE = FKBHNEMDBMK.NMJDMHNMDNJ,
            OUTLINE_WHITE_ON_MESH = FKBHNEMDBMK.NCDCDOBCCJG,
            NO_SHADOW = FKBHNEMDBMK.HIEILGEFEDA,
            SHARED_MATERIAL = FKBHNEMDBMK.DJKFKKODCCM,
            MATERIAL_FROM_CHARACTER = FKBHNEMDBMK.KNJBKIMEFMF,
        }

        public static readonly ModelInstruction NONE = Enum.NONE;
        public static readonly ModelInstruction OUTLINE_WHITE_ON_MESH = Enum.OUTLINE_WHITE_ON_MESH;
        public static readonly ModelInstruction NO_SHADOW = Enum.NO_SHADOW;
        public static readonly ModelInstruction SHARED_MATERIAL = Enum.SHARED_MATERIAL;
        public static readonly ModelInstruction MATERIAL_FROM_CHARACTER = Enum.MATERIAL_FROM_CHARACTER;
#endregion wrapper
    }
}
