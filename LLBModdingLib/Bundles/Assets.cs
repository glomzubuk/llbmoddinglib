using System;
using System.Collections;
using System.Linq;
using LLHandlers;
using UnityEngine;

namespace LLBML.Bundles
{
    /// <summary>
    /// A wrapper to the game's obfuscated Assets class
    /// </summary>
    public static class Assets
    {
#pragma warning disable 1591
        #region wrapper

        public static void AddSkinVariants() => JPLELOFJOOH.MHOHIACFLOH();
        public static void CharacterToIconSpriteIndex(Character character) => JPLELOFJOOH.LPCPPJOIIEF(character);
        public static IEnumerable ECharacters() => JPLELOFJOOH.MOFHLDPOIEJ();

        public static AOOJOMIECLD GetCharacterModelValues(Character character, CharacterVariant variant = CharacterVariant.DEFAULT) => JPLELOFJOOH.NEBGBODHHCG(character, variant);
        public static string GetCharacterName(Character character) => JPLELOFJOOH.OAGHLPGCAOI(character);

        public static T LoadFromBundle<T>(Bundle bundle, string asset, bool noWarning = false) where T : UnityEngine.Object => JPLELOFJOOH.PFKGPLFILIO<T>(bundle, asset, noWarning);
        public static T LoadFromResources<T>(string path, string name, bool noWarning = false) where T : UnityEngine.Object => JPLELOFJOOH.NAMFPIPFAHP<T>(path, name, noWarning);

        public static T SpawnFromBundle<T>(Bundle bundle, string asset) where T : UnityEngine.Object => JPLELOFJOOH.ICBBOHPGIFM<T>(bundle, asset);
        public static GameObject SpawnScreen(ScreenType screenType) => JPLELOFJOOH.HNHBCLJGPCE(screenType);
        public static GameObject SpawnDebugHudUber() => JPLELOFJOOH.OEKMFNNLHHI();

        public static VariantInfo[] variantInfos
        {
            get => Array.ConvertAll(JPLELOFJOOH.LKIFMPEFNGB, input => (VariantInfo)input);
            set => JPLELOFJOOH.LKIFMPEFNGB = Array.ConvertAll(value, input => (JPLELOFJOOH.NCBHPNHFLAJ)input);
        }
        public static MeshInfo[] meshInfos
        {
            get => Array.ConvertAll(JPLELOFJOOH.OGAHHGABFPE, input => (MeshInfo)input);
            set => JPLELOFJOOH.OGAHHGABFPE = Array.ConvertAll(value, input => (JPLELOFJOOH.GHKGDLBCFPK)input);
        }
        #endregion wrapper
#pragma warning restore 1591
    }
}
