namespace LLBML.Bundles
{
    /// <summary>
    /// A wrapper to the game's obfuscated Assets.VariantInfo struct
    /// </summary>
    public class VariantInfo
    {
#pragma warning disable 1591
        private readonly JPLELOFJOOH.NCBHPNHFLAJ _variantInfo;

        public VariantInfo(JPLELOFJOOH.NCBHPNHFLAJ vi)
        {
            this._variantInfo = vi;
        }
        public VariantInfo(Character character, CharacterVariant variant, string model, string[] materials, DLC dlc = DLC.NONE)
        {
            this._variantInfo = new JPLELOFJOOH.NCBHPNHFLAJ(character, variant, model, materials, ModelInstruction.NONE, dlc);
        }
        public VariantInfo(Character character, CharacterVariant variant, string model, string[] materials, ModelInstruction instruction, DLC dlc = DLC.NONE)
        {
            if (instruction == null) instruction = ModelInstruction.NONE;
            this._variantInfo = new JPLELOFJOOH.NCBHPNHFLAJ(character, variant, model, materials, instruction, dlc);
        }
        public VariantInfo(Character character, CharacterVariant variant, string model, string material, DLC dlc = DLC.NONE)
        {
            this._variantInfo = new JPLELOFJOOH.NCBHPNHFLAJ(character, variant, model, material, ModelInstruction.NONE, dlc);
        }
        public VariantInfo(Character character, CharacterVariant variant, string model, string material,  ModelInstruction instruction, DLC dlc = DLC.NONE)
        {
            if (instruction == null) instruction = ModelInstruction.NONE;
            this._variantInfo = new JPLELOFJOOH.NCBHPNHFLAJ(character, variant, model, material, instruction, dlc);
        }
        public override bool Equals(object obj) => (obj is JPLELOFJOOH.NCBHPNHFLAJ || obj is VariantInfo) && Equals((JPLELOFJOOH.NCBHPNHFLAJ)obj, (JPLELOFJOOH.NCBHPNHFLAJ)this);
        public override int GetHashCode() => _variantInfo.GetHashCode();
        public override string ToString(){
            return $"VariantInfo ({character} | {variant} | {model} | {instruction} | {dlc})";
        }
        public static implicit operator JPLELOFJOOH.NCBHPNHFLAJ(VariantInfo vi) => vi._variantInfo;
        public static implicit operator VariantInfo(JPLELOFJOOH.NCBHPNHFLAJ vi) => new VariantInfo(vi);

        #region wrapper
        public Character character => _variantInfo.LALEEFJMMLH;
        public CharacterVariant variant => _variantInfo.AIINAIDBHJI;
        public string model => _variantInfo.BADNNNCEKEF;
        public string[] materials => _variantInfo.MJJFEAGJICG;
        public ModelInstruction instruction => _variantInfo.MIGKFLKFCIJ;
        public DLC dlc => _variantInfo.DDHGFDLLDAG;
        #endregion wrapper
#pragma warning restore 1591
    }
}
