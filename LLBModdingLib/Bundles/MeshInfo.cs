namespace LLBML.Bundles
{
    /// <summary>
    /// A wrapper to the game's obfuscated Assets.MeshInfo struct
    /// </summary>
    public class MeshInfo
    {
#pragma warning disable 1591
        private readonly JPLELOFJOOH.GHKGDLBCFPK _meshInfo;

        public MeshInfo(JPLELOFJOOH.GHKGDLBCFPK mi)
        {
            this._meshInfo = mi;
        }
        public MeshInfo(string model, float scale, int offset)
        {
            this._meshInfo = new JPLELOFJOOH.GHKGDLBCFPK(model, scale, offset);
        }

        public override bool Equals(object obj) => (obj is JPLELOFJOOH.GHKGDLBCFPK || obj is MeshInfo) && Equals((JPLELOFJOOH.GHKGDLBCFPK)obj, (JPLELOFJOOH.GHKGDLBCFPK)this);
        public override int GetHashCode() => _meshInfo.GetHashCode();
        public override string ToString(){
            return $"MeshInfo ({model} : {scale} | {offset})";
        }
        public static implicit operator JPLELOFJOOH.GHKGDLBCFPK(MeshInfo mi) => mi._meshInfo;
        public static implicit operator MeshInfo(JPLELOFJOOH.GHKGDLBCFPK mi) => new MeshInfo(mi);

        #region wrapper
        public static float scaleFactor => JPLELOFJOOH.GHKGDLBCFPK.LINLLJFBAKF;

        public string model => _meshInfo.BADNNNCEKEF;
        public float scale => _meshInfo.OGMOEKPOBBP;
        public int offset => _meshInfo.AFPIBJADFLD;

        #endregion wrapper
#pragma warning restore 1591
    }
}
