using LLHandlers;

namespace LLBML.Bundles
{
    /// <summary>
    /// A wrapper to the game's obfuscated ModelValues struct
    /// </summary>
    public class ModelValues
    {
#pragma warning disable 1591
        private readonly AOOJOMIECLD _modelValues;

        public ModelValues(AOOJOMIECLD mv)
        {
            this._modelValues = mv;
        }

        public override bool Equals(object obj) => (obj is AOOJOMIECLD || obj is ModelValues) && Equals((AOOJOMIECLD)obj, (AOOJOMIECLD)this);
        public override int GetHashCode() => _modelValues.GetHashCode();
        public override string ToString(){
            return $"ModelValues ({assetModel} | {bundle} | {instruction} | {offset} | {scale})";
        }
        public static implicit operator AOOJOMIECLD(ModelValues mv) => mv._modelValues;
        public static implicit operator ModelValues(AOOJOMIECLD mv) => new ModelValues(mv);

        #region wrapper
        public string assetModel => _modelValues.DKADFKGPGHL;
        public string[] assetMaterials => _modelValues.IODEGBOKAAK;
        public Bundle bundle => _modelValues.EDKLFODCINA;
        public int offset => _modelValues.AFPIBJADFLD;
        public float scale => _modelValues.OGMOEKPOBBP;
        public ModelInstruction instruction => _modelValues.MIGKFLKFCIJ;




        #endregion wrapper
#pragma warning restore 1591
    }
}
