﻿using System;
using System.Collections.Generic;
using System.Linq;
using LLBML.Utils;


namespace LLBML
{
    /// <summary>
    /// A class to hold the <see cref="Character"/> enum related functions.
    /// </summary>
    public class CharacterApi
    {
        /// <summary>
        /// The invalid characters defined as characters you can't select through normal gameplay.
        /// </summary>
        public static Character[] invalidCharacters =
        {
            Character._MAX_NORMAL,
            Character.DUMMY,
            Character.NONE,
        };

        /// <summary>
        /// The unplayable characters characters defined as characters you can select but can't play.
        /// </summary>
        public static Character[] unplayableCharacters =
        {
            Character.RANDOM,
        };

        /// <summary>
        /// Gets an Enumerator containing all <see cref="Character"/> enum values.
        /// </summary>
        /// <returns>All characters.</returns>
        public static IEnumerable<Character> GetAllCharacters()
        {
            //TODO Implement getting custom characters here
            return GenericUtils.GetEnumValues<Character>();
        }

        /// <summary>
        /// Gets an Enumerator containing the <see cref="Character"/> enum values that represent valid characters.
        /// </summary>
        /// <returns>All valid characters.</returns>
        public static IEnumerable<Character> GetValidCharacters()
        {
            return GetAllCharacters().Except(invalidCharacters);
        }

        /// <summary>
        /// Gets an Enumerator containing the <see cref="Character"/> enum values that represent playable characters.
        /// </summary>
        /// <returns>All valid characters.</returns>
        public static IEnumerable<Character> GetPlayableCharacters()
        {
            return GetValidCharacters().Except(unplayableCharacters);
        }

        /// <summary>
        /// Gets the <see cref="Character"/> enum value by name.
        /// </summary>
        /// <returns>The Character enum value.</returns>
        /// <param name="characterName">Character name.</param>
        public static Character GetCharacterByName(string characterName)
        {
            for (int i = 0; i < (int)Character._MAX_NORMAL; i++)
            {
                Character currentCharacter = (Character)i;
                if (currentCharacter.ToString() == characterName.ToUpper())
                {
                    return currentCharacter;
                }
            }
            return Character.NONE;
        }

        /// <inheritdoc cref="GetCharacterByName(string)"/>
        /// <remarks>
        /// This version uses <see cref="GenericUtils.GetEnumValues{T}"/> instead of looping 
        /// </remarks>
        public static Character TheWitcherGetCharacterByName(string characterName)
        {
            foreach (Character character in GenericUtils.GetEnumValues<Character>())
            {
                if (character.ToString() == characterName)
                {
                    return character;
                }
            }
            return Character.NONE;
        }
    }
}
