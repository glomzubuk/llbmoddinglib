﻿using System;
using System.Linq;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using Multiplayer;
using Steamworks;
using LLBML.Utils;
using BepInEx.Logging;

namespace LLBML.Networking
{
    public enum TransactionState
    {
        Sending,
        Receiving,
        Done
    }

    public enum TransactionCode : byte
    {
        Start = 0,
        Fragment = 1,
        Ack = 2
    }
    public class Transaction : IEnumerable, IByteable
    {
        private static ManualLogSource Logger = LLBMLPlugin.Log;

        internal const uint TransactionOverhead = sizeof(byte) + sizeof(ushort) + sizeof(uint);
        internal const uint MaxFragmentSize = NetworkApi.maxSteamworksReliablePacket - TransactionOverhead;
        internal static Dictionary<string, Transaction> transactionCache = new Dictionary<string, Transaction>();
        internal static ushort transactionCount = 0;

        public string id { get; private set; }

        private readonly ushort index;
        private readonly int channel;
        private readonly CSteamID sender;
        private readonly CSteamID receiver;
        private readonly uint fragmentSize;
        private byte[][] fragments;
        private uint currentFragment;


        public Transaction(ushort index, CSteamID sender, CSteamID receiver, byte[] data, int channel = (int)Channel.Vanilla, uint fragmentSize = Transaction.MaxFragmentSize, TransactionState state = TransactionState.Sending)
        {
            this.id = GetID(sender, receiver, index);
            this.index = index;
            this.channel = channel;
            this.sender = sender;
            this.receiver = receiver;
            this.fragmentSize = fragmentSize;
            uint nbFragments = (uint)System.Math.Ceiling(data.Length / (decimal)fragmentSize);
            fragments = new byte[nbFragments][];
            Logger.LogDebug($"Creating transaction from payload with size {data.Length} and with {nbFragments} fragments.");
            for (uint i = 0; i < nbFragments; i++)
            {
                Logger.LogDebug($"Splitting fragment number {i}");

                byte[] fragment = new byte[fragmentSize];
                uint startIndex = i * fragmentSize;

                long copyLength = data.Length - startIndex - fragmentSize >= 0 ? fragmentSize : data.Length - startIndex;
                Array.Copy(data, i * fragmentSize, fragment, 0, copyLength);
                fragments[i] = fragment;

            }
            this.currentFragment = 0;
        }
        public Transaction(ushort index, CSteamID sender, CSteamID receiver, uint nbFragments, int channel, uint fragmentSize)
        {
            this.id = GetID(sender, receiver, index);
            this.index = index;
            this.channel = channel;
            this.sender = sender;
            this.receiver = receiver;
            this.fragmentSize = fragmentSize;
            fragments = new byte[nbFragments][];
            currentFragment = 0;
        }

        public static Transaction Create(Envelope envelope)
        {
            CSteamID sender = NetworkApi.PeerIDToCSteamID(envelope.sender);
            CSteamID receiver = NetworkApi.PeerIDToCSteamID(envelope.receiver);

            Transaction transaction = new Transaction(transactionCount++, sender, receiver, envelope.ToBytes(), (int)Channel.Vanilla);
            transactionCache.Add(transaction.id, transaction);
            return transaction;
        }
        public static Transaction Create(CSteamID destination, byte[] data, int channel = (int)Channel.Vanilla)
        {
            CSteamID local = NetworkApi.PeerIDToCSteamID(P2P.localPeer.peerId);
            Transaction transaction = new Transaction(transactionCount++, local, destination, data, channel);
            transactionCache.Add(transaction.id, transaction);
            return transaction;
        }

        public static string GetID(CSteamID sender, CSteamID receiver, int index)
        {
            return String.Format("{0}_{1}_{2:00000}", sender, receiver, index);
        }

        internal Dictionary<TransactionCode, Action<CSteamID, byte[]>> TCodeMapping = new Dictionary<TransactionCode, Action<CSteamID, byte[]>>
        {
            [TransactionCode.Start] = ReceiveStart,
            [TransactionCode.Fragment] = ReceiveFragment,
            [TransactionCode.Ack] = ReceiveAck,
        };

        internal void Start()
        {
            byte[] transaction_data = new byte[] { (byte)TransactionCode.Start };
            transaction_data.Add(this.ToBytes());
            Logger.LogDebug($"Transaction {id} start!");

            NetworkApi.SendP2PPacket(receiver, transaction_data, EP2PSend.k_EP2PSendUnreliable, (int)Channel.Transaction, false);
        }

        internal static void ReceiveStart(CSteamID sender, byte[] data)
        {
            Transaction newTransaction = Transaction.FromBytes(data);
            transactionCache.Add(newTransaction.id, newTransaction);
            Logger.LogDebug($"Received transaction start for transaction {newTransaction.id}, start!");
            newTransaction.SendAck(-1);
        }


        internal void SendNextFragment()
        {
            if (currentFragment > fragments.Length) return;

            SendFragment(currentFragment);
            currentFragment++;
        }
        internal void SendFragment(uint fragmentId)
        {
            byte[] transaction_data = new byte[] { (byte)TransactionCode.Fragment };

            byte[] fragment_data = GetFragment(fragmentId);
            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (BinaryWriter binaryWriter = new BinaryWriter(memoryStream))
                {
                    binaryWriter.Write(index);
                    binaryWriter.Write(fragmentId);
                    binaryWriter.Write(fragment_data.Length);
                    binaryWriter.Write(fragment_data);
                    transaction_data.Add(memoryStream.ToArray());
                }
            }
            Logger.LogDebug($"Sending fragment {fragmentId} for transaction {id}.");
            NetworkApi.SendP2PPacket(receiver, transaction_data, EP2PSend.k_EP2PSendReliable, (int)Channel.Transaction, false);
        }


        internal static void ReceiveFragment(CSteamID sender, byte[] data)
        {
            using (MemoryStream memoryStream = new MemoryStream(data))
            {
                using (BinaryReader binaryReader = new BinaryReader(memoryStream))
                {
                    ushort index = binaryReader.ReadUInt16();
                    uint fragmentId = binaryReader.ReadUInt32();
                    int fragmentSize = binaryReader.ReadInt32();
                    byte[] fragmentData = binaryReader.ReadBytes(fragmentSize);

                    CSteamID local = NetworkApi.PeerIDToCSteamID(P2P.localPeer.peerId);
                    string tId = GetID(sender, local, data[0]);
                    Transaction transaction = transactionCache[tId];

                    Logger.LogDebug($"Received fragment {fragmentId} for transaction {tId}.");
                    transaction.AddFragment(fragmentId, fragmentData);
                    transaction.SendAck((int)fragmentId);

                    if (transaction.GetMissingFragments().Count == 0)
                    {
                        transaction.End();
                    }
                }
            }
        }

        internal void SendAck(int fragmentId)
        {

            byte[] ack_data = new byte[] { (byte)TransactionCode.Ack };
            ack_data.Add(this.ToBytes());
            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (BinaryWriter binaryWriter = new BinaryWriter(memoryStream))
                {
                    binaryWriter.Write(index);
                    binaryWriter.Write(fragmentId);
                    ack_data.Add(memoryStream.ToArray());
                }
            }

            Logger.LogDebug($"Sending Ack {fragmentId} for transaction {id}.");
            NetworkApi.SendP2PPacket(receiver, ack_data, EP2PSend.k_EP2PSendUnreliable, (int)Channel.Transaction, false);
        }

        internal static void ReceiveAck(CSteamID sender, byte[] data)
        {

            using (MemoryStream memoryStream = new MemoryStream(data))
            {
                using (BinaryReader binaryReader = new BinaryReader(memoryStream))
                {
                    ushort index = binaryReader.ReadUInt16();
                    int fragmentId = binaryReader.ReadInt32();

                    CSteamID local = NetworkApi.PeerIDToCSteamID(P2P.localPeer.peerId);
                    string tId = GetID(sender, local, data[0]);
                    Transaction transaction = transactionCache[tId];

                    Logger.LogDebug($"Received Ack {fragmentId} for transaction {transaction.id}.");
                    Logger.LogDebug($"Sending next fragment...");
                    if (fragmentId == -1)
                    {
                        transaction.currentFragment = 0;
                        transaction.SendNextFragment();
                    }
                    else
                    {
                        transaction.SendNextFragment();
                    }
                }
            }

        }

        internal void End()
        {
            byte[] finalData = fragments.SelectMany(byteArr => byteArr).ToArray();
            var channelMapping = Networking_Patches.NetSteamPatch.channelMapping;
            Channel channel = (Channel)this.channel;
            if (channelMapping.ContainsKey(channel)) {
                channelMapping[channel].Invoke(finalData, (uint)finalData.Length, this.sender);
            }
            else
            {
                // TODO Call steam's receive packet method on the appropriate channel
            }
        }

        public void AddFragment (uint fragmentId, byte[] fragment)
        {
            if (fragment.Length > fragmentSize)
            {
                throw new InvalidOperationException($"Received a fragment too big for transaction {this.id}. Fragment was {fragment.Length} byte long, expected {fragmentSize} max");
            }
            if (fragmentId == currentFragment)
            {
                fragments[currentFragment] = fragment;
                currentFragment++;
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        public List<int> GetMissingFragments()
        {
            return fragments.Select((fragment, index) => fragment == null ? index : -1).Where(index => index != -1).ToList();
        }


        public byte[] GetFragment(uint fragmentId)
        {
            return fragments[fragmentId];
        }

        public IEnumerator GetEnumerator()
        {
            return fragments.SelectMany(fragment => fragment).GetEnumerator();
        }


        public byte[] ToBytes()
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (BinaryWriter binaryWriter = new BinaryWriter(memoryStream))
                {
                    binaryWriter.Write(index);
                    binaryWriter.Write((uint)fragments.Length);
                    binaryWriter.Write(fragmentSize);
                    binaryWriter.Write(sender.ToString());
                    binaryWriter.Write(receiver.ToString());
                    binaryWriter.Write(channel);
                    return memoryStream.ToArray();
                }
            }
        }

        public static Transaction FromBytes(byte[] data)
        {
            using (MemoryStream memoryStream = new MemoryStream(data))
            {
                using (BinaryReader binaryReader = new BinaryReader(memoryStream))
                {
                    ushort index = binaryReader.ReadUInt16();
                    uint fragmentCount = binaryReader.ReadUInt32();
                    uint fragmentSize = binaryReader.ReadUInt32();
                    CSteamID sender = NetworkApi.PeerIDToCSteamID(binaryReader.ReadString());
                    CSteamID receiver = NetworkApi.PeerIDToCSteamID(binaryReader.ReadString());
                    int channel = binaryReader.ReadInt32();

                    return new Transaction(index, sender, receiver, fragmentCount, channel, fragmentSize);

                }
            }
        }
    }
}
