﻿using System;
using LLBML.Utils;

namespace LLBML.Networking
{
    /// <summary>
    /// A wrapper to the game's obfuscated EP2PSend enum
    /// </summary>
    public class EP2PSend : EnumWrapper<HKNEJCDBFGF>
    {
#pragma warning disable 1591
        EP2PSend(int id) : base(id) { }
        EP2PSend(HKNEJCDBFGF gameState) : base((int)gameState) { }

        public override string ToString() => ((Enum)this.id).ToString();


        public static implicit operator HKNEJCDBFGF(EP2PSend ew) => (HKNEJCDBFGF)ew.id;
        public static implicit operator EP2PSend(HKNEJCDBFGF val) => new EP2PSend(val);
        public static implicit operator EP2PSend(int id) => new EP2PSend(id);
        public static implicit operator EP2PSend(Enum val) => new EP2PSend((int)val);
        public static implicit operator Enum(EP2PSend ew) => (Enum)ew.id;

        #region wrapper
        public enum Enum
        {
            k_EP2PSendUnreliable,
            k_EP2PSendUnreliableNoDelay,
            k_EP2PSendReliable,
            k_EP2PSendReliableWithBuffering,
        }

        public enum Enum_Mapping
        {
            k_EP2PSendUnreliable = HKNEJCDBFGF.DNJICPLBGMN,
            k_EP2PSendUnreliableNoDelay = HKNEJCDBFGF.BNOKLLLOANL,
            k_EP2PSendReliable = HKNEJCDBFGF.KNFJEJCAILI,
            k_EP2PSendReliableWithBuffering = HKNEJCDBFGF.IKOMHOFHKMC,
        }


        public static readonly EP2PSend k_EP2PSendUnreliable = Enum.k_EP2PSendUnreliable;
        public static readonly EP2PSend k_EP2PSendUnreliableNoDelay = Enum.k_EP2PSendUnreliableNoDelay;
        public static readonly EP2PSend k_EP2PSendReliable = Enum.k_EP2PSendReliable;
        public static readonly EP2PSend k_EP2PSendReliableWithBuffering = Enum.k_EP2PSendReliableWithBuffering;
        #endregion
#pragma warning restore 1591
    }
}

