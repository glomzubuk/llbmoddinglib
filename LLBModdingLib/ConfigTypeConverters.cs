﻿using System;
using System.Runtime.CompilerServices;
using BepInEx.Configuration;
using UnityEngine;

namespace LLBML
{
    /// <summary>
    /// Additionnal config types
    /// </summary>
    internal static class ConfigTypeConverters
    {
        public static void AddConfigConverters()
        {
            AddColorConverters();
            AddVectorConverters();
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        private static void AddColorConverters()
        {


            TypeConverter colorConverter = new TypeConverter
            {
                ConvertToString = (obj, type) => ColorUtility.ToHtmlStringRGBA((Color)obj),
                ConvertToObject = (str, type) =>
                {
                    if (!ColorUtility.TryParseHtmlString("#" + str.Trim('#', ' '), out var c))
                        throw new FormatException("Invalid color string, expected hex #RRGGBBAA");
                    return c;
                },
            };
            TomlTypeConverter.AddConverter(typeof(Color), colorConverter);
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        private static void AddVectorConverters()
        {
            var jsonConverter = new TypeConverter
            {
                ConvertToString = (obj, type) => JsonUtility.ToJson(obj),
                ConvertToObject = (str, type) => JsonUtility.FromJson(type: type, json: str),
            };

            TomlTypeConverter.AddConverter(typeof(Vector2), jsonConverter);
            TomlTypeConverter.AddConverter(typeof(Vector3), jsonConverter);
            TomlTypeConverter.AddConverter(typeof(Vector4), jsonConverter);
            TomlTypeConverter.AddConverter(typeof(Quaternion), jsonConverter);
        }
    }
}
