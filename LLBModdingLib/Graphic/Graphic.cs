﻿using System;
using UnityEngine;

namespace LLBML
{
    public static class GraphicUtils
    {
        private static readonly Texture2D backgroundTexture = Texture2D.whiteTexture;
        private static readonly GUIStyle textureStyle = new GUIStyle { normal = new GUIStyleState { background = backgroundTexture } };
        public static void DrawRect(Rect position, Color color)
        {
            DrawRectWithContent(position, color, null);
        }
        public static void DrawRectWithContent(Rect position, Color color, GUIContent content)
        {
            Color previousBGColor = GUI.backgroundColor;
            GUI.backgroundColor = color;
            GUI.Box(position, content ?? GUIContent.none, textureStyle);
            GUI.backgroundColor = previousBGColor;
        }
    }
}
