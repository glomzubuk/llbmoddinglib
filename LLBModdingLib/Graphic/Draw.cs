﻿using System;
using UnityEngine;
using LLBML.Math;

namespace LLBML.Graphic
{
    public static class Draw
    {
        private static Material _lineMaterial = null;
        private static Material lineMaterial
        {
            get
            {
                if (_lineMaterial == null)
                {
                    Shader shader = Shader.Find("Hidden/Internal-Colored");
                    _lineMaterial = new Material(shader);
                    _lineMaterial.hideFlags = HideFlags.HideAndDontSave;
                    _lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                    _lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                    _lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
                    _lineMaterial.SetInt("_ZWrite", 0);
                    _lineMaterial.SetInt("_ZTest", (int)UnityEngine.Rendering.CompareFunction.Always);
                }
                return _lineMaterial;
            }
        }

        public static void Cube(Vector2f center, Vector2f size, Color color, float thicc = 4f, Draw.Alignment align = Draw.Alignment.INSIDE)
        {
            Draw.lineMaterial.SetPass(0);
            thicc *= 0.01f;
            Camera gameplayCam = GameCamera.gameplayCam;
            GL.PushMatrix();
            GL.Begin(GL.QUADS);
            GL.Color(color);
            GL.LoadProjectionMatrix(gameplayCam.projectionMatrix);
            Vector3 vector  = new Vector3(center.x - (size.x * HHBCPNCDNDH.GMEDDLALMGA), center.y - (size.y * HHBCPNCDNDH.GMEDDLALMGA));
            Vector3 vector2 = new Vector3(center.x - (size.x * HHBCPNCDNDH.GMEDDLALMGA), center.y + (size.y * HHBCPNCDNDH.GMEDDLALMGA));
            Vector3 vector3 = new Vector3(center.x + (size.x * HHBCPNCDNDH.GMEDDLALMGA), center.y + (size.y * HHBCPNCDNDH.GMEDDLALMGA));
            Vector3 vector4 = new Vector3(center.x + (size.x * HHBCPNCDNDH.GMEDDLALMGA), center.y - (size.y * HHBCPNCDNDH.GMEDDLALMGA));
            Vector3 b = new Vector3(thicc, 0f, 0f);
            Vector3 b2 = new Vector3(0f, thicc * (float)align, 0f);
            Vector3 b3 = new Vector3(thicc, thicc, 0f);
            Vector3 b4 = new Vector3(thicc, -thicc, 0f);
            GL.Vertex(vector - b2);
            GL.Vertex(vector2 + b2);
            if (align == Draw.Alignment.INSIDE)
            {
                GL.Vertex(vector2 + b);
                GL.Vertex(vector + b);
            }
            else
            {
                GL.Vertex(vector2 - b4);
                GL.Vertex(vector - b3);
            }
            GL.Vertex(vector2);
            GL.Vertex(vector3);
            if (align == Draw.Alignment.INSIDE)
            {
                GL.Vertex3(vector3.x, vector3.y - thicc, vector3.z);
                GL.Vertex3(vector2.x, vector2.y - thicc, vector2.z);
            }
            else
            {
                GL.Vertex3(vector3.x, vector3.y + thicc, vector3.z);
                GL.Vertex3(vector2.x, vector2.y + thicc, vector2.z);
            }
            GL.Vertex(vector3 + b2);
            GL.Vertex(vector4 - b2);
            if (align == Draw.Alignment.INSIDE)
            {
                GL.Vertex(vector4 - b);
                GL.Vertex(vector3 - b);
            }
            else
            {
                GL.Vertex(vector4 + b4);
                GL.Vertex(vector3 + b3);
            }
            GL.Vertex(vector4);
            GL.Vertex(vector);
            if (align == Draw.Alignment.INSIDE)
            {
                GL.Vertex3(vector.x, vector.y + thicc, vector.z);
                GL.Vertex3(vector4.x, vector4.y + thicc, vector4.z);
            }
            else
            {
                GL.Vertex3(vector.x, vector.y - thicc, vector.z);
                GL.Vertex3(vector4.x, vector4.y - thicc, vector4.z);
            }
            GL.End();
            GL.PopMatrix();
        }

        public static void Polygon(Vector2f center, Floatf radius, Color color, int vertexNumber = 360, bool hollow = false, float thicness = 4f)
        {
            Draw.lineMaterial.SetPass(0);
            thicness *= 0.01f;
            Camera gameplayCam = GameCamera.gameplayCam;
            float insideRadius = radius - thicness <= 0f ? 0f : ((float)radius - thicness);
            GL.PushMatrix();

            if (hollow) GL.Begin(GL.TRIANGLES); 
            else GL.Begin(GL.QUADS); 

            GL.Color(color);
            GL.LoadProjectionMatrix(gameplayCam.projectionMatrix);
            float angle = 2 * Mathf.PI / vertexNumber;
            for (float segment = 0; segment <= vertexNumber; segment++)
            {
                float cur_angle = (angle * segment) - Mathf.PI / 2 + angle / 2;
                float next_angle = angle * (segment + 1f) - Mathf.PI / 2 + angle / 2;
                if (hollow)
                {
                    GL.Vertex3(center.x, center.y, 0f);
                    GL.Vertex3(center.x + Mathf.Cos(cur_angle) * radius, center.y + Mathf.Sin(cur_angle) * radius, 0f);
                    GL.Vertex3(center.x + Mathf.Cos(next_angle) * radius, center.y + Mathf.Sin(next_angle) * radius, 0f);
                }
                else
                {
                    GL.Vertex3(center.x + Mathf.Cos(cur_angle) * radius, center.y + Mathf.Sin(cur_angle) * radius, 0f);
                    GL.Vertex3(center.x + Mathf.Cos(next_angle) * radius, center.y + Mathf.Sin(next_angle) * radius, 0f);
                    GL.Vertex3(center.x + Mathf.Cos(next_angle) * insideRadius, center.y + Mathf.Sin(next_angle) * insideRadius, 0f);
                    GL.Vertex3(center.x + Mathf.Cos(cur_angle) * insideRadius, center.y + Mathf.Sin(cur_angle) * insideRadius, 0f);
                }
            }
            GL.End();
            GL.PopMatrix();
        }

        public enum Alignment : byte
        {
            INSIDE,
            OUTSIDE
        }
    }
}
