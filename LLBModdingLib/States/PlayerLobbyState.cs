﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using HarmonyLib;
using LLBML.Utils;
using LLBML.Players;
using LLBML.Networking;

namespace LLBML.States
{
    /// <summary>
    /// A wrapper to the game's obfuscated PlayerLobbyState class
    /// </summary>
    public class PlayerLobbyState
    {
#pragma warning disable 1591
        #region wrapper
        private readonly JMLEHJLKPAC _pls;
        public PlayerLobbyState()
        {
            this._pls = new JMLEHJLKPAC();
        }
        public PlayerLobbyState(JMLEHJLKPAC pls)
        {
            this._pls = pls;
        }
        public PlayerLobbyState(Player player)
        {
            this._pls = new JMLEHJLKPAC(player);
        }
        public override bool Equals(object obj) => (obj is JMLEHJLKPAC || obj is PlayerLobbyState) && Equals((JMLEHJLKPAC)obj, (JMLEHJLKPAC)this);
        public override int GetHashCode() => _pls.GetHashCode();
        public override string ToString() => _pls.ToString();
        public static implicit operator JMLEHJLKPAC(PlayerLobbyState pls) => pls._pls;
        public static implicit operator PlayerLobbyState(JMLEHJLKPAC pls) => new PlayerLobbyState(pls);

        public void CopyTo(Player p) => _pls.LMIPEFAPKOJ(p);
        public void WriteBytes(BinaryWriter writer) => _pls.JFFMEHKLMNG(writer);
        public static PlayerLobbyState ReadBytes(BinaryReader reader) => JMLEHJLKPAC.JFNLMPNOEMA(reader);

        public int playerNr { get => _pls.BKEOPDPFFPM; set => _pls.BKEOPDPFFPM = value; }
        public Character character { get => _pls.LALEEFJMMLH; set => _pls.LALEEFJMMLH = value; }
        public CharacterVariant variant { get => _pls.AIINAIDBHJI; set => _pls.AIINAIDBHJI = value; }
        public Team team { get => _pls.HEOKEMBMDIJ; set => _pls.HEOKEMBMDIJ = value; }
        public bool selected { get => _pls.CHNGAKOIJFE; set => _pls.CHNGAKOIJFE = value; }
        public bool ready { get => _pls.GFCMODHPPCN; set => _pls.GFCMODHPPCN = value; }
        public bool spectator { get => _pls.HOKENEBPEGH; set => _pls.HOKENEBPEGH = value; }
        #endregion
#pragma warning restore 1591

        #region additions

        internal static readonly byte[] nullNetID = new byte[] { 0, 0, 0, 0 };
        internal static Dictionary<string, PLSCustomPayload> customPayloads = new Dictionary<string, PLSCustomPayload>();

        internal static void Patch(Harmony harmonyInstance)
        {
            harmonyInstance.PatchAll(typeof(PlayerLobbyState_Patches));
        }
        internal struct PLSCustomPayload
        {
            public string name;
            public byte[] netID;
            public Func<PlayerLobbyState, byte[]> onSendPayload;
            public Action<PlayerLobbyState, byte[]> onReceivePayload;

            public PLSCustomPayload(byte[] netID, Func<PlayerLobbyState, byte[]> onSendPayload, Action<PlayerLobbyState, byte[]> onReceivePayload)
            {
                this.name = null;
                this.netID = netID;
                this.onSendPayload = onSendPayload;
                this.onReceivePayload = onReceivePayload;
            }

            public PLSCustomPayload(BepInEx.PluginInfo pi, Func<PlayerLobbyState, byte[]> onSendPayload, Action<PlayerLobbyState, byte[]> onReceivePayload)
            {
                this.name = pi.Metadata.Name;
                this.netID = NetworkApi.GetNetworkIdentifier(pi.Metadata.GUID); ;
                this.onSendPayload = onSendPayload;
                this.onReceivePayload = onReceivePayload;
            }
        }

        /// <summary>
        /// Register a custom packing and unpacking methods that will be called each time a <see cref="PlayerLobbyState"/> is sent and received
        /// </summary>
        /// <param name="info">The <see cref="BepInEx.PluginInfo"/> of the requesting plugin</param>
        /// <param name="onSendPayload">The method to call when packing data to send a <see cref="PlayerLobbyState"/></param>
        /// <param name="onReceivePayload">The method to call when unpacking data from a <see cref="PlayerLobbyState"/></param>
        public static void RegisterPayload(BepInEx.PluginInfo info, Func<PlayerLobbyState, byte[]> onSendPayload, Action<PlayerLobbyState, byte[]> onReceivePayload)
        {
            byte[] netID = NetworkApi.GetNetworkIdentifier(info.Metadata.GUID);
            string index = StringUtils.PrettyPrintBytes(netID);
            if (customPayloads.ContainsKey(index))
            {
                LLBMLPlugin.Log.LogWarning($"{info.Metadata.Name} has already registered a payload.");
                return;
            }
            customPayloads.Add(index, new PLSCustomPayload(info, onSendPayload, onReceivePayload));
        }

        /// <summary>
        /// Removes the <see cref="PlayerLobbyState"/> payload associated with a plugin
        /// </summary>
        /// <param name="info">The <see cref="BepInEx.PluginInfo"/> of the requesting plugin</param>
        public static void UnregisterPayload(BepInEx.PluginInfo info)
        {
            byte[] netID = NetworkApi.GetNetworkIdentifier(info.Metadata.GUID);
            string index = StringUtils.PrettyPrintBytes(netID);

            if (customPayloads.ContainsKey(index))
            {
                customPayloads.Remove(index);
            }
            else
            {
                LLBMLPlugin.Log.LogWarning($"{info.Metadata.Name} doesn't have a registered payload.");
                DebugUtils.PrintStacktrace();
            }
        }
        #endregion

    }


#pragma warning disable 1591
    public static class PlayerLobbyState_Patches
    {
        [HarmonyPatch(typeof(JMLEHJLKPAC), nameof(JMLEHJLKPAC.JFFMEHKLMNG))]
        [HarmonyPrefix]
        public static bool WriteBytes_Prefix(BinaryWriter __0, JMLEHJLKPAC __instance)
        {
            if (PlayerLobbyState.customPayloads.Count <= 0) return true;

            PlayerLobbyState instance = __instance;
            __0.Write((byte)instance.playerNr);
            __0.Write((byte)instance.character);
            __0.Write((byte)instance.variant);
            __0.Write((byte)instance.team);
            __0.Write((byte)((!instance.selected) ? 0 : 1));
            __0.Write((byte)((!instance.ready) ? 0 : 1));
            __0.Write((byte)((!instance.spectator) ? 0 : 1));

            foreach(var modPayload in PlayerLobbyState.customPayloads)
            {
                byte[] rawPayload = null;
                try
                {
                    LLBMLPlugin.Log.LogDebug($"Test1: {modPayload.Value.name}");
                    rawPayload = modPayload.Value.onSendPayload(instance);
                }
                catch (Exception e)
                {
                    LLBMLPlugin.Log.LogError($"{modPayload.Value.name ?? modPayload.Value.netID.ToString()} returned error during PlayerLobbyState packing: " + e);
                }

                if (rawPayload != null && rawPayload.Length != 0)
                {
                    __0.Write(modPayload.Value.netID);
                    __0.Write((byte)rawPayload.Length);
                    __0.Write(rawPayload);
                }
            }

            __0.Write(PlayerLobbyState.nullNetID);
            return false;
        }

        [HarmonyPatch(typeof(JMLEHJLKPAC), nameof(JMLEHJLKPAC.JFNLMPNOEMA))]
        [HarmonyPrefix]
        public static bool ReadBytes_Prefix(ref JMLEHJLKPAC __result, BinaryReader __0 )
        {
            if (PlayerLobbyState.customPayloads.Count <= 0) return true;
            __result = new PlayerLobbyState()
            {
                playerNr = (int)__0.ReadByte(),
                character = (Character)__0.ReadByte(),
                variant = (CharacterVariant)__0.ReadByte(),
                team = (Team)__0.ReadByte(),
                selected = (__0.ReadByte() != 0),
                ready = (__0.ReadByte() != 0),
                spectator = (__0.ReadByte() != 0)
            };


            while(__0.BaseStream.Position < __0.BaseStream.Length)
            {
                LLBMLPlugin.Log.LogDebug($"Current Stream position: {__0.BaseStream.Position}/{__0.BaseStream.Length}");

                byte[] netID = __0.ReadBytes(4);
                if (netID.Aggregate(0, (acc, val) => acc + val) == 0)
                {
                    break;
                }
                int payloadLength = __0.ReadByte();
                byte[] payload = __0.ReadBytes(payloadLength);

                string index = StringUtils.PrettyPrintBytes(netID);
                if (PlayerLobbyState.customPayloads.ContainsKey(index))
                {
                    var modPayload = PlayerLobbyState.customPayloads[index];
                    try
                    {
                        modPayload.onReceivePayload(__result, payload);
                    }
                    catch(Exception e)
                    {
                        LLBMLPlugin.Log.LogError($"{modPayload.name ?? modPayload.netID.ToString()} returned error during PlayerLobbyState unpacking: " + e);
                    }
                }
                else
                {
                    
                    LLBMLPlugin.Log.LogWarning($"Unknown PluginID: Couldn't find a plugin with \"{StringUtils.PrettyPrintBytes(netID)}\" as netID. It had {payloadLength} bytes of data.");
                    //LLBMLPlugin.Log.LogError("As of now, this results in a corrupted stream we can't continue with. Fix your shit Glom!.");
                    // TODO Should be good? Test more!
                    continue;
                }
            }
            return false;
        }
    }
#pragma warning restore 1591
}
