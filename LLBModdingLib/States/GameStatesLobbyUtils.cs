﻿using System;
using LLBML.Players;
using BepInEx.Logging;

namespace LLBML.States
{
    /// <summary>
    /// Useful methods to interact with Lobby states
    /// </summary>
    public static class GameStatesLobbyUtils
    {
        private static ManualLogSource Logger = LLBMLPlugin.Log;

        #region Lobby
        /// <summary>
        /// Gets the current GameStatesLobby object.
        /// </summary>
        /// <exception cref="System.InvalidOperationException">Thrown when the method is called when there's no instanciated lobby object.</exception>
        /// <returns>The current GameStatesLobby object</returns>
        public static HPNLMFHPHFD GetLobby()
        {
            if (!GameStates.IsInLobby())
            {
                throw new InvalidOperationException(ErrorMessage("GameStatesLobby", GameStates.GetCurrent().ToString()));
            }
            return (HPNLMFHPHFD)GameStates.GetCurrentGameStateObject();
        }

        /// <summary>
        /// Updates the state's data with data from the provided player.
        /// </summary>
        /// <param name="gs_lobby">The current lobby state.</param>
        /// <param name="player">The player to update info from.</param>
        /// <param name="play_selection_anim">Whether the character selection animation be played.</param>
        public static void UpdatePlayer(HPNLMFHPHFD gs_lobby, Player player, bool play_selection_anim = false)
            => gs_lobby.BDMIDGAHNLA(player, play_selection_anim);

        /// <inheritdoc cref="UpdatePlayer(HPNLMFHPHFD, Player, bool)"/>
        /// <summary>
        /// Updates the current state's data with data from the provided player.
        /// </summary>
        /// <exception cref="System.InvalidOperationException">Thrown when the method is called when there's no instanciated lobby object.</exception>
        public static void UpdatePlayer(Player player, bool play_selection_anim = false)
            => UpdatePlayer(GetLobby(), player, play_selection_anim);
        #endregion

        #region LobbyOnline
        /// <summary>
        /// Gets the current GameStatesLobbyOnline object.
        /// </summary>
        /// <exception cref="System.InvalidOperationException">Thrown when the method is called when there's no instanciated online lobby object.</exception>
        /// <returns>The current GameStatesLobbyOnline object</returns>
        public static HDLIJDBFGKN GetOnlineLobby()
        {
            if (!GameStates.IsInOnlineLobby())
            {
                throw new InvalidOperationException(ErrorMessage("GameStatesLobbyOnline", GameStates.GetCurrent().ToString()));
            }
            return HDLIJDBFGKN.instance;
        }

        /// <summary>
        /// Sets ready information and updates clients with the new state.
        /// </summary>
        /// <param name="gs_lobbyOnline">The current online lobby state.</param>
        /// <param name="ready">Should ready be set.</param>
        /// <param name="resetTimer">Should the autoready timer be reset and ran again.</param>
        public static void MakeSureReadyIs(HDLIJDBFGKN gs_lobbyOnline, bool ready, bool resetTimer = false)
            => gs_lobbyOnline.IKPDLPDNHIJ(ready, resetTimer);

        /// <inheritdoc cref="MakeSureReadyIs(HDLIJDBFGKN, bool, bool)"/>
        /// <exception cref="System.InvalidOperationException">Thrown when the method is called when there's no instanciated online lobby object.</exception>
        public static void MakeSureReadyIs(bool ready, bool resetTimer = false)
            => MakeSureReadyIs(GetOnlineLobby(), ready, resetTimer);

        /// <summary>
        /// Sends the provided player's data to the other clients.
        /// </summary>
        /// <param name="gs_lobbyOnline">The current online lobby state.</param>
        /// <param name="player">The player to pull info from.</param>
        public static void SendPlayerState(HDLIJDBFGKN gs_lobbyOnline, Player player)
            => gs_lobbyOnline.OFGNNIBJOLH(player);

        /// <inheritdoc cref="MakeSureReadyIs(HDLIJDBFGKN, bool, bool)"/>
        /// <exception cref="System.InvalidOperationException">Thrown when the method is called when there's no instanciated online lobby object.</exception>
        public static void SendPlayerState(Player player)
            => SendPlayerState(GetOnlineLobby(), player);

        /// <summary>
        /// Updates the state from the player's data and sends it to the other clients
        /// </summary>
        /// <param name="player">The player to update the state from.</param>
        /// <exception cref="System.InvalidOperationException">Thrown when the method is called when there's no instanciated online lobby object.</exception>
        public static void RefreshPlayerState(Player player)
        {
            Logger.LogInfo("Refreshing player " + player.nr);
            if (GameStates.IsInLobby())
            {

                UpdatePlayer(player, false);
                /*
                bool isOnlineLobby = GameStates.IsInOnlineLobby();
                if (isOnlineLobby)
                {
                    Logger.LogInfo("Lobby is online, sending player state");
                    SendPlayerState(player);
                }*/
            }
            else
            {
                Logger.LogWarning("Tried to refresh state while not in a lobby.\n Current GameState: " + GameStates.GetCurrent() + " . Stacktrace: \n" +
                    new System.Diagnostics.StackTrace());
            }
        }

        /// <inheritdoc cref="RefreshPlayerState(Player)"/>
        public static void RefreshLocalPlayerState() => RefreshPlayerState(Player.GetLocalPlayer());

        /// <summary>
        /// Gets the autoready status from the current online lobby state.
        /// </summary>
        /// <returns>Whether or not the autoready is currently enabled.</returns>
        /// <exception cref="System.InvalidOperationException">Thrown when the method is called when there's no instanciated online lobby object.</exception>
        public static bool GetAutoreadyStatus() {
            if (GameStates.IsInLobby() && GameStates.IsInOnlineLobby())
            {
                return GetOnlineLobby().BOEPIJPONCK;
            }
            return false;
        }
        #endregion

        #region LobbyLocal
        /// <summary>
        /// Gets the current GameStatesLobbyLocal object.
        /// </summary>
        /// <exception cref="System.InvalidOperationException">Thrown when the method is called when there's no instanciated local lobby object.</exception>
        /// <returns>The current GameStatesLobbyLocal object</returns>
        public static IJDANPONMLL GetLocalLobby()
        {
            if (!GameStates.IsInLocalLobby())
            {
                throw new InvalidOperationException(ErrorMessage("GameStatesLobbyLocal", GameStates.GetCurrent().ToString()));
            }
            return (IJDANPONMLL)GameStates.GetCurrentGameStateObject();
        }
        #endregion

        #region LobbySingle
        /// <summary>
        /// Gets the current GameStatesLobbySingle object.
        /// </summary>
        /// <exception cref="System.InvalidOperationException">Thrown when the method is called when there's no instanciated single lobby object.</exception>
        /// <returns>The current GameStatesLobbySingle object</returns>
        public static HFAEJNGHDDM GetSingleLobby()
        {
            if (!GameStates.IsInSingleLobby())
            {
                throw new InvalidOperationException(ErrorMessage("GameStatesLobbySingle", GameStates.GetCurrent().ToString()));
            }
            return (HFAEJNGHDDM)GameStates.GetCurrentGameStateObject();
        }
        #endregion

        private static string ErrorMessage(string obName, string state)
        {
            return "Tried to fetch "+ obName +" object while not in the right state. Current state: " + state;
        }

    }
}
