﻿using System;
using LLBML.Utils;

namespace LLBML.States
{
    /// <summary>
    /// A wrapper to the game's obfuscated GameState enum
    /// </summary>
    public class GameState : EnumWrapper<JOFJHDJHJGI>
    {
#pragma warning disable 1591
        GameState(int id) : base(id) { }
        GameState(JOFJHDJHJGI gameState) : base((int)gameState) { }

        public override string ToString() => ((Enum)this.id).ToString();


        public static implicit operator JOFJHDJHJGI(GameState ew) => (JOFJHDJHJGI)ew.id;
        public static implicit operator GameState(JOFJHDJHJGI val) => new GameState(val);
        public static implicit operator GameState(int id) => new GameState(id);
        public static implicit operator GameState(Enum val) => new GameState((int)val);
        public static implicit operator Enum(GameState ew) => (Enum)ew.id;


        #region wrapper
        public enum Enum
        {
            NONE,
            INTRO,
            MENU,
            QUIT,
            LOBBY_LOCAL,
            LOBBY_ONLINE,
            LOBBY_CHALLENGE,
            CHALLENGE_LADDER,
            CHALLENGE_LOST,
            STORY_GRID,
            STORY_COMIC,
            LOBBY_TRAINING,
            LOBBY_TUTORIAL,
            CREDITS,
            OPTIONS_GAME,
            OPTIONS_INPUT,
            OPTIONS_AUDIO,
            OPTIONS_VIDEO,
            GAME_INTRO,
            GAME,
            GAME_PAUSE,
            GAME_RESULT,
            UNLOCKS,
            LOBBY_STORY,
            UNKNOWN1,
            UNKNOWN2,
        }
        public enum Enum_Mapping
        {
            NONE = JOFJHDJHJGI.NMJDMHNMDNJ,
            INTRO = JOFJHDJHJGI.GJABHCPDPLL,
            MENU = JOFJHDJHJGI.LFHHNPALNNB,
            QUIT = JOFJHDJHJGI.EOCBBKOIFNO,
            LOBBY_LOCAL = JOFJHDJHJGI.FADIBJAINDE,
            LOBBY_ONLINE = JOFJHDJHJGI.NBNDONMABLK,
            LOBBY_CHALLENGE = JOFJHDJHJGI.PMAEHDMCGKD,
            CHALLENGE_LADDER = JOFJHDJHJGI.KLECIDHGOGK,
            CHALLENGE_LOST = JOFJHDJHJGI.JHPAMACNJOB,
            STORY_GRID = JOFJHDJHJGI.LKPCIDDIPED,
            STORY_COMIC = JOFJHDJHJGI.HPAKOHDNANP,
            LOBBY_TRAINING = JOFJHDJHJGI.ABGFICGIPAD,
            LOBBY_TUTORIAL = JOFJHDJHJGI.OHOGGLLLFEB,
            CREDITS = JOFJHDJHJGI.FCPICJNNEDB,
            OPTIONS_GAME = JOFJHDJHJGI.OJNLIDICJLA,
            OPTIONS_INPUT = JOFJHDJHJGI.BKJBCPJEMBH,
            OPTIONS_AUDIO = JOFJHDJHJGI.MEGBCOEMOBD,
            OPTIONS_VIDEO = JOFJHDJHJGI.FEBKOEMINOA,
            GAME_INTRO = JOFJHDJHJGI.FJBICGGMJNG,
            GAME = JOFJHDJHJGI.CDOFDJMLGLO,
            GAME_PAUSE = JOFJHDJHJGI.LGILIJKMKOD,
            GAME_RESULT = JOFJHDJHJGI.AOEDPBCIAEP,
            UNLOCKS = JOFJHDJHJGI.OENIHJACJNJ,
            LOBBY_STORY = JOFJHDJHJGI.ODAMAGAJDIG,
            UNKNOWN1 = JOFJHDJHJGI.NBIHJEGEIML,
            UNKNOWN2 = JOFJHDJHJGI.OHDDNFCEECN,
        }


        public static readonly GameState NONE = Enum.NONE;
        public static readonly GameState INTRO = Enum.INTRO;
        public static readonly GameState MENU = Enum.MENU;
        public static readonly GameState QUIT = Enum.QUIT;
        public static readonly GameState LOBBY_LOCAL = Enum.LOBBY_LOCAL;
        public static readonly GameState LOBBY_ONLINE = Enum.LOBBY_ONLINE;
        public static readonly GameState LOBBY_CHALLENGE = Enum.LOBBY_CHALLENGE;
        public static readonly GameState CHALLENGE_LADDER = Enum.CHALLENGE_LADDER;
        public static readonly GameState CHALLENGE_LOST = Enum.CHALLENGE_LOST;
        public static readonly GameState STORY_GRID = Enum.STORY_GRID;
        public static readonly GameState STORY_COMIC = Enum.STORY_COMIC;
        public static readonly GameState LOBBY_TRAINING = Enum.LOBBY_TRAINING;
        public static readonly GameState LOBBY_TUTORIAL = Enum.LOBBY_TUTORIAL;
        public static readonly GameState CREDITS = Enum.CREDITS;
        public static readonly GameState OPTIONS_GAME = Enum.OPTIONS_GAME;
        public static readonly GameState OPTIONS_INPUT = Enum.OPTIONS_INPUT;
        public static readonly GameState OPTIONS_AUDIO = Enum.OPTIONS_AUDIO;
        public static readonly GameState OPTIONS_VIDEO = Enum.OPTIONS_VIDEO;
        public static readonly GameState GAME_INTRO = Enum.GAME_INTRO;
        public static readonly GameState GAME = Enum.GAME;
        public static readonly GameState GAME_PAUSE = Enum.GAME_PAUSE;
        public static readonly GameState GAME_RESULT = Enum.GAME_RESULT;
        public static readonly GameState UNLOCKS = Enum.UNLOCKS;
        public static readonly GameState LOBBY_STORY = Enum.LOBBY_STORY;
        public static readonly GameState UNKNOWN1 = Enum.UNKNOWN1;
        public static readonly GameState UNKNOWN2 = Enum.UNKNOWN2;
        #endregion
#pragma warning restore 1591
    }
}
