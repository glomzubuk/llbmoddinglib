﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Multiplayer;
using HarmonyLib;
using BepInEx;
using BepInEx.Logging;
using LLBML.Utils;
using LLBML.Networking;

namespace LLBML.Messages
{

    /// <summary>
    /// A class to send, receive and edit messages
    /// </summary>
    public static class MessageApi
    {
        private static ManualLogSource Logger = LLBMLPlugin.Log;
        internal static List<int> vanillaMessageCodes; //TODO this is supposed to be a list of bytes, but casting enum to bytes is problematic, figure out later
        internal static List<int> internalMessageCodes;
        internal static Dictionary<byte, List<CustomMessage>> vanillaSubscriptions = new Dictionary<byte, List<CustomMessage>>();
        internal static Dictionary<ushort, CustomMessage> customMessages = new Dictionary<ushort, CustomMessage>();

        public delegate void OnReceiveMessageHandler(object sender, Message e);

        internal static void Patch(Harmony harmonyPatcher)
        {
            vanillaMessageCodes = GenericUtils.GetEnumValues<Msg>().Cast<int>().ToList();
            internalMessageCodes = GenericUtils.GetEnumValues<SpecialCode>().Cast<int>().ToList();
        }

        /// <summary>
        /// Register a custom message that will be assiociated with the provided code and call the provided method when received 
        /// </summary>
        /// <param name="pluginInfo">The <see cref="PluginInfo"/> of the requesting plugin</param>
        /// <param name="messageCode">The code to register</param>
        /// <param name="messageName">The name/reason for the message code</param>
        /// <param name="onReceiveMessageCallback">The method to call when receiving the message</param>
        public static CustomMessage RegisterCustomMessage(PluginInfo pluginInfo, ushort messageCode, string messageName, Action<Message> onReceiveMessageCallback)
        {
            return RegisterCustomMessage(pluginInfo, messageCode, messageName, new MessageActions(onReceiveMessageCallback));
        }
        /// <summary>
        /// Register a custom message that will be assiociated with the provided code and call the various actions contained into it's <see cref="MessageActions"/> 
        /// </summary>
        /// <param name="pluginInfo">The <see cref="PluginInfo"/> of the requesting plugin</param>
        /// <param name="messageCode">The code to register</param>
        /// <param name="messageName">The name/reason for the message code</param>
        /// <param name="messageActions">A <see cref="MessageActions"/> containing the actions to do when a message with the provided code is received</param>
        public static CustomMessage RegisterCustomMessage(PluginInfo pluginInfo, ushort messageCode, string messageName, MessageActions messageActions)
        {
            if(messageCode <= 255)
            {
                Logger.LogWarning("Message codes under 256 are reserved for the game (" + ((Msg)messageCode).ToString() + ").");
                Logger.LogWarning("Could not register code action.");
                return null;
            }
            if (customMessages.ContainsKey(messageCode))
            {
                Logger.LogWarning("Message code number" + messageCode + " is already taken by another mod (" + customMessages[messageCode].messageName + ").");
                Logger.LogWarning("Could not register code action.");
                return null;
            }
            CustomMessage cm = new CustomMessage(pluginInfo, messageCode, messageName, messageActions);
            customMessages.Add(messageCode, cm);
            Logger.LogDebug("Registered message code number " + cm.messageCode + " for " + cm.pluginInfo.Metadata.Name + ". Reason: " + cm.messageName + ". Callback name: " + cm.messageActions.onReceiveCode.Method.Name);

            return cm;
        }

        /// <summary>
        /// Removes the custom message associated with a code
        /// </summary>
        /// <param name="pluginInfo">The <see cref="PluginInfo"/> of the requesting plugin</param>
        /// <param name="messageCode">The code to unregister</param>
        /// <returns>True if the message was successfuly unregistered, false otherwise</returns>
        public static bool UnregisterCustomMessage(PluginInfo pluginInfo, ushort messageCode)
        {
            if (customMessages.ContainsKey(messageCode))
            {
                if (pluginInfo.Metadata.GUID == customMessages[messageCode].pluginInfo.Metadata.GUID)
                {
                    customMessages.Remove(messageCode);
                    Logger.LogDebug("Removed code number " + messageCode);
                    return true;
                }
                else
                {
                    Logger.LogWarning("Tried to unregister a code with the wrong plugin info. Ignoring.");
                }
            }
            else
            {
                Logger.LogWarning("No message code number " + messageCode + " previously registered");
            }
            return false;
        }

        /// <summary>
        /// Subscribe to a vanilla message and calls back the actions in the <see cref="MessageActions"/> 
        /// </summary>
        /// <param name="pluginInfo">The <see cref="PluginInfo"/> of the requesting plugin</param>
        /// <param name="messageCode">The code to subscribe to</param>
        /// <param name="messageName">The name/reason for the message code</param>
        /// <param name="onReceiveMessageCallback">The method to call when receiving the message</param>
        public static CustomMessage SubscribeToVanillaMessage(PluginInfo pluginInfo, Msg messageCode, string messageName, Action<Message> onReceiveMessageCallback)
        {
            return SubscribeToVanillaMessage(pluginInfo, messageCode, messageName, new MessageActions(onReceiveMessageCallback));
        }
        /// <summary>
        /// Subscribe to a vanilla message and calls back the actions in the <see cref="MessageActions"/> 
        /// </summary>
        /// <param name="pluginInfo">The <see cref="PluginInfo"/> of the requesting plugin</param>
        /// <param name="messageCode">The code to subscribe to</param>
        /// <param name="messageName">The name/reason for the message code</param>
        /// <param name="messageActions">A <see cref="MessageActions"/> containing the actions to do when a message with the provided code is received</param>
        public static CustomMessage SubscribeToVanillaMessage(PluginInfo pluginInfo, Msg messageCode, string messageName, MessageActions messageActions)
        {
            if (!vanillaSubscriptions.ContainsKey((byte)messageCode))
            {
                vanillaSubscriptions[(byte)messageCode] = new List<CustomMessage>();
            }
            CustomMessage cm = new CustomMessage(pluginInfo, (byte)messageCode, messageName, messageActions);
            vanillaSubscriptions[(byte)messageCode].Add(cm);
            Logger.LogDebug("Registered message code number " + cm.messageCode + " for " + cm.pluginInfo.Metadata.Name + ". Reason: " + cm.messageName + ". Callback name: " + cm.messageActions.onReceiveCode.Method.Name);

            return cm;
        }

        /// <summary>
        /// Removes the custom message associated with a code
        /// </summary>
        /// <param name="pluginInfo">The <see cref="PluginInfo"/> of the requesting plugin</param>
        /// <param name="messageCode">The code that was subscribe to</param>
        /// <param name="customMessage">The customMessage to unregister</param>
        /// <returns>True if the message was successfuly unregistered, false otherwise</returns>
        public static bool UnsubscribeToVanillaMessage(PluginInfo pluginInfo, Msg messageCode, CustomMessage customMessage)
        {
            if (vanillaSubscriptions.ContainsKey((byte)messageCode))
            {
                int cmIndex = vanillaSubscriptions[(byte)messageCode].IndexOf(customMessage);

                if (cmIndex == -1)
                {
                    Logger.LogWarning("No custom message matching " + customMessage + " previously registered");
                    return false;
                }

                if (vanillaSubscriptions[(byte)messageCode][cmIndex].pluginInfo.Metadata.GUID == pluginInfo.Metadata.GUID)
                {
                    customMessages.Remove((byte)messageCode);
                    Logger.LogDebug("Removed code number " + messageCode);
                    return true;
                }
                else
                {
                    Logger.LogWarning("Tried to unregister a code with the wrong plugin info. Ignoring.");
                }
            }
            else
            {
                Logger.LogWarning("No message code number " + messageCode + " previously registered");
            }
            return false;
        }
    }


    public class MessageEventArgs : EventArgs
    {
        public Message Message { get; private set; }

        public MessageEventArgs(Message message)
        {
            Message = message;
        }
    }
}
