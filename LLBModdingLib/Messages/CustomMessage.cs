﻿using System;
using System.IO;
using Multiplayer;
using BepInEx;

namespace LLBML.Messages
{
    public class CustomMessage
    {
        public readonly PluginInfo pluginInfo;
        public readonly int messageCode;
        public readonly string messageName;
        public readonly MessageActions messageActions;

        public CustomMessage(PluginInfo pluginInfo, int messageCode, string messageName, Action<Message> onReceiveCode)
        {
            this.pluginInfo = pluginInfo;
            this.messageCode = messageCode;
            this.messageActions = new MessageActions(onReceiveCode);
        }


        public CustomMessage(PluginInfo pluginInfo, int messageCode, string messageName, MessageActions messageActions)
        {
            this.pluginInfo = pluginInfo;
            this.messageCode = messageCode;
            this.messageActions = messageActions;
        }
    }

    public class MessageActions
    {
        public readonly Action<Message> onReceiveCode;
        public readonly Action<BinaryWriter, object, int> customWriter;
        public readonly Action<BinaryReader, Message> customReader;

        public MessageActions(Action<Message> onReceiveCode, Action<BinaryWriter, object, int> customWriter = null, Action<BinaryReader, Message> customReader = null)
        {
            this.onReceiveCode = onReceiveCode;
            this.customWriter = customWriter;
            this.customReader = customReader;
        }
    }
}
