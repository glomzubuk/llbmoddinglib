﻿using System;
using System.Collections.Generic;
namespace LLBML.Math
{
    /// <summary>
    /// Utils for binary operations.
    /// </summary>
    public static class BinaryUtils
    {
        /// <summary>
        /// Takes both half of a <see cref="T:byte[]"/> and apply a XOR operation between them, resulting in a half length array.
        /// </summary>
        /// <exception cref="NotImplementedException">Will throw for arrays that are not of lenght that is a power of 2</exception>
        /// <returns>The resulting folded <see cref="T:byte[]"/>.</returns>
        /// <param name="input">The array to fold.</param>
        /// <param name="times">The number of times to fold the array.</param>
        public static byte[] XORFold(byte[] input, int times = 1)
        {
            if (input.Length % 2 != 0) throw new NotImplementedException();
            int half_length = input.Length / 2;

            byte[] result = new byte[half_length];
            for (int i = 0; i < input.Length; i++)
            {
                if (i < half_length)
                {
                    result[i] = input[i];
                }
                else
                {
                    result[i - half_length] ^= input[i];
                }
            }
            if (times > 1)
            {
                return XORFold(result, times - 1);
            }
            return result;
        }


        /// <summary>
        /// Determines whether a specified instance of <see cref="T:byte[]"/> is equal to another specified <see cref="T:byte[]"/>.
        /// </summary>
        /// <param name="b1">The first <see cref="T:byte[]"/> to compare.</param>
        /// <param name="b2">The second <see cref="T:byte[]"/> to compare.</param>
        /// <returns><c>true</c> if <c>hash1</c> and <c>hash2</c> are equal; otherwise, <c>false</c>.</returns>
        public static bool ByteArraysEqual(byte[] b1, byte[] b2)
        {
            if (b1 == b2) return true;
            if (b1 == null || b2 == null) return false;
            if (b1.Length != b2.Length) return false;
            for (int i = 0; i < b1.Length; i++)
            {
                if (b1[i] != b2[i]) return false;
            }
            return true;
        }
    }
}
