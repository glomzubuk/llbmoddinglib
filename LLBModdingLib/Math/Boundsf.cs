﻿using System;
namespace LLBML.Math
{
    /// <summary>
    /// A wrapper to the game's obfuscated Boundsf struct
    /// </summary>
    public struct Boundsf
    {
#pragma warning disable 1591
        private readonly JEPKNLONCHD _boundsf;

        public Boundsf(JEPKNLONCHD b)
        {
            this._boundsf = b;
        }

        public override bool Equals(object obj)
        {
            return (obj is JEPKNLONCHD || obj is Boundsf) && Equals((JEPKNLONCHD)obj, (JEPKNLONCHD)this);
        }
        public override int GetHashCode()
        {
            return _boundsf.GetHashCode();
        }

        public Boundsf(Vector2f _center, Vector2f _size) => this._boundsf = new JEPKNLONCHD(_center, _size);

        public static implicit operator JEPKNLONCHD(Boundsf b) => b._boundsf;
        public static implicit operator Boundsf(JEPKNLONCHD b) => new Boundsf(b);

        public override string ToString() => _boundsf.ToString();
        public string ToString(string format) => _boundsf.FIEOOJHNDNF(format);

        public Vector2f center => _boundsf.LOLBPNFNKMI;
        public Vector2f extents => _boundsf.JGFCBKOCFFA;
        public Vector2f max => _boundsf.KHBAPGIEOIC;
        public Vector2f min => _boundsf.MOGDHBGHAOA;
        public Vector2f size => _boundsf.IACOKHPMNGN;

        public bool Intersects(Boundsf against) => _boundsf.GJNBHIPKIFH(against);

#pragma warning restore 1591
        }
}
