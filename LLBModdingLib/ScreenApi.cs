﻿using System;
using System.Reflection;
using LLScreen;
using HarmonyLib;

namespace LLBML
{
    public static class ScreenApi
    {
        //public static ScreenBase[] CurrentScreens { get; internal set; } = AccessTools.StaticFieldRefAccess<ScreenBase[]>(typeof(UIScreen), "currentScreens");
        public static ScreenBase[] CurrentScreens => UIScreen.currentScreens;

        public static bool IsGameResult() => CurrentScreens[0]?.screenType == ScreenType.GAME_RESULTS;
        public static PostScreen GetPostScreen()
        {
            if (IsGameResult())
            {
                return CurrentScreens[0] as PostScreen;
            }
            return null;
        }
    }
}
